# Teaching Material for HTL Bulme

Most likely, you're here to read some teaching material. The renedered content
is available at https://bulme.find-santa.eu.

If you want to contribute an improvement, fork this repository. After cloning
your fork, install the dependencies:

    npm install

This repository uses the @11ty/eleventy static site generator. It can be
executed via

    npm run build

or even easier viewing your changes live via

    npm run serve

Once complete, check in and push your changes and create a pull request against
this repository. Thank you.

## Shortcodes
include_code
: create a link to pythontutor including a given filename's source code for
  stepwise execution and visualization

isodate
: the current date (compile time)

year
: enter the current year (compile time)

## Paired Shortcodes
answer
: add space for an answer that may include an optional solution
