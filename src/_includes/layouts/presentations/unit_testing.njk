{#
Using extends instead of normal layout chaining is necessary here as
most of the content is shared between multiple unit test intros for different
programming languages with only a few blocks being distinct.
#}

{% extends "layouts/presentation.njk" %}
{% set source_path = "source/" + extension + "/unit_testing/" %}
{% set data_path = "/data/" + extension + "/unit_testing/" %}

{% block presentation %}
<section>
  <h2>Purpose of Unit Tests</h2>
  <section>
    <h3>What is a Unit Test</h3>
    <ul>
      <li>Allows automated testing of independent code units</li>
      <li>No replacement for other test categories</li>
      <li>Write them as early as possible - if possible before the code it tests</li>
      <li>Tests can serve as useful requirements specification</li>
      <li>Create a test for each bug you encounter (refacturing!)</li>
    </ul>
  </section>
  <section>
    <h3>Which problems does unit testing solve?</h3>
    <p>Code without tests (aka. legacy code) is extremely difficult to work
      with</p>
    <div class="fragment">
      <p>Unit tests...</p>
      <ul>
        <li>are a first line of defense against bugs</li>
        <li>improve code quality by dictating testable code</li>
        <li>allow releasing higher quality early and often</li>
        <li>give more confidence to programmers</li>
        <li>are a prerequisit to refactoring</li>
        <li>can be used as a contract with regard to desired behavior</li>
        <li>prevent regressions (bugs in features priorly working properly)</li>
      </ul>
    </div>
  </section>
  <section>
    <h3>Which problems does unit testing cause?</h3>
    <p>Essentially none, but writing unit tests ...</p>
    <ul class="fragment">
      <li>seems to cost time</li>
      <li>demands discipline</li>
      <li>is far from trivial</li>
      <li>does not substitute other kinds of testing</li>
    </ul>
  </section>
</section>


<section>
  <section>
  <h2>What is a Test(case)</h2>
  <ul>
    <li>Answers a single question about the code it is testing</li>
    <li>Run completely by itself (automation)</li>
    <li>Results must not require human interpretation</li>
    <li>Should be independent of other tests and their order of execution</li>
    <li>Can serve as documentation of how (not) to use your code</li>
  </ul>
  </section>

  <section>
    <h3>Flaky Tests</h3>
    <ul>
      <li>Have different outcomes without changes to the code</li>
      <li>Erode confidence in testing processes</li>
      <li>Fix them (mute / skip them until they are fixed) or delete them</li>
    </ul>
  </section>
</section>

<section>
  <h2>Software Quality</h2>
  <section>
    <h3>User's view (visible to the user)</h3>
    <ul>
      <li>Functionality</li>
      <li>Reliability</li>
      <li>Usability</li>
      <li>Performance/ efficiency</li>
    </ul>
  </section>
  <section>
    <h3>Producer's view</h3>
    <ul>
      <li>Portability</li>
      <li>Maintainability</li>
      <li>Testability (vs. legacy code)</li>
      <li>Transparency (is the code readable/ understandable?)</li>
    </ul>
  </section>
</section>

<section>
  <h2>We Need a Unit Testing Framework</h2>
  <p>For writing, compiling and running unit tests, a unit testing framework
    is required.
  </p>
  <p class="fragment">There are many concurring
    <a href="https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks#{{ language }}">
      unit testing frameworks for {{ language }}</a>.
  </p>
{% block framework %}{% endblock %}
</section>

<section>
  <h2>Example: Refactor a Function</h2>
  <section>
{% highlight extension %}
{% include source_path + "before_refactoring." + extension %}
{% endhighlight %}
{% download data_path + "before_refactoring." + extension %}

<div class="fragment">
<p>After experiencing performance problems, we want to
  use a faster algorithm
</p>

{% highlight extension %}
{% include source_path + "after_refactoring." + extension %}
{% endhighlight %}
{% download data_path + "after_refactoring." + extension %}
</div>
  </section>
  <section>
    <p>How can we apply the change without risking to destroy our
      application?</p>
    <ol class="fragment">
      <li>Define good test cases</li>
      <li>Implement test cases using your project's unit testing library</li>
      <li>Run the tests before the change to ensure they pass</li>
      <li>Apply the change, then run the tests again</li>
    </ol>
  </section>

  <section>
    <h3>1. Define Good Test Cases</h3>
    <p>Using pencil and paper / brainstorming: define relevant test cases</p>
    <ul>
      <li>All code paths should be used (discussable)</li>
      <li>Add one or more common cases</li>
      <li>Include relevant edge cases</li>
    </ul>

    <aside class="notes">
      <p>Discuss in class whether writing tests for code paths is really
        smart.</p>
    </aside>
  </section>

  <section>
    <h3>Possible Test Cases</h3>
    <p>25 is a possible common case.
      Add 26 as an even input. 500000000 is a possible large input.
      The edge cases should include 0 and 4294967295.
      Ideally, also add 1, 2, 4294967293 and 4294967294.<br />
    </p>

    <p class="fragment">Expected results for these are 325, 351,
      125000000250000000, 0 and 9223372034707292160 as well as
      1, 3, 9223372026117357571 and 9223372030412324865.</p>

{% if extension in ['c', 'cpp'] %}
    <p class="fragment">Note that 4294967295 does not terminate with the current
      implementation.
    </p>
{% endif %}
  </section>

  <section>
    <h3>2. Implement defined test cases</h3>
{% highlight extension %}
{% include source_path + framework_path + "sum_to_test." + extension %}
{% endhighlight %}
{% download data_path + framework_path + "sum_to_test." + extension %}
  </section>

  <section>
    <h3>3. Run the Unittests Before the Change</h3>
{% block run_tests_before %}{% endblock %}
  </section>

  <section>
    <h3>4. Apply the Change, then Run the Tests Again</h3>
    <p>For the sake of the presentation, instead of applying the change,
      we use another source file.
    </p>
{% block run_tests_after %}{% endblock %}
{% if extension in ['c', 'cpp'] %}
    <p class="fragment">Now the tests are fast, but they are failing.
      Fix the code, then compile and run the tests again.
    </p>
{% endif %}

    <aside class="notes">
{% if extension in ['c', 'cpp'] %}
      <p>There are two issues:</p>

      <ol>
        <li>The cast cuts away 0.5 before the multiplication
          (for odd inputs).</li>
{% else %}
      <ol>
{% endif %}
        <li>For very large inputs, working with doubles is not
          feasible anymore at perfect precision.</li>
      </ol>
      {% download data_path + "after_refactoring_fixed." + extension %}
    </aside>
  </section>
</section>

{% block about_framework %}{% endblock %}


<section>
  <h2>Test-driven development (TDD)</h2>
  <p>TDD is a methodology that emphasizes writing tests before the
    actual code</p>
  <p>Helps ensure that code is tested thoroughly and avoids regressions</p>

  <div class="fragment">
    <p><strong>Repeat the cycle for each new piece of functionality</strong>
    </p>
    <ol>
      <li>Write a failing test that describes the desired functionality</li>
      <li>Write the minimum amount of code necessary to make the test pass</li>
      <li>Refactor the code to improve its structure and maintainability</li>
      <li>Make sure that all tests pass</li>
    </ol>
  </div>
</section>


<section class="center">
  <h2>
    <q>Whenever you are tempted to type something into a <code>print</code>
    statement or a debugger expression, write it as a test instead.</q><br />
    <cite>– Martin Fowler</cite>
  </h2>
</section>

{% include 'slides/closing.njk' %}
{% endblock %}
