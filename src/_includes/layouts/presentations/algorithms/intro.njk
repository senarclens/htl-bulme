{#
requires: functions
#}

{#
Using extends instead of normal layout chaining is necessary here as
most of the content is shared between a C and C++ version of the presentation.
#}

{% extends 'layouts/presentation.njk' %}

{% set title = 'Algorithms in ' + language %}
{% set subtitle = 'Introduction' %}

{# subtitle differs between C and C++ and is defined in these presentations #}
{% set directory = 'algorithms/' %}

{% set source_path = 'source/' + extension + '/' + directory %}
{% set data_path = '/data/' + extension + '/' + directory  %}
{% set unit_testing_path = 'source/' + extension + '/unit_testing/' %}

{% block presentation %}


<section>
  <h2>Terminology</h2>
  <section>
    <h3>Data Structure</h3>
    <ul>
      <li>Stores and organizes data so that it can be used efficiently</li>
      <li><a href="http://en.wikipedia.org/wiki/Data_structure">
        http://en.wikipedia.org/wiki/Data_structure
      </a></li>
    </ul>
  </section>
  <section>
    <h3>Algorithm</h3>
    <ul>
      <li>Procedure or formula for solving a problem</li>
      <li>Finite series of computational steps to produce a result</li>
      <li>Independent from any programming language</li>
      <li>Examples for basic algorithms: sorting and searching</li>
      <li>Brute force algorithm: performs an exhaustive search of all possible solutions</li>
    </ul>
  </section>
  <section>
    <h3>Complexity</h3>
    <ul>
      <li>Measure of the difficulty of constructing a solution to a problem</li>
      <li>Estimate of the number of operations needed to execute an algorithm</li>
    </ul>
  </section>
  <section>
    <h3>Big O notation</h3>
    <ul>
      <li>Asymptotic notation</li>
      <li>Estimates the worst case performance of data structures and algorithms</li>
      <li><a href="http://en.wikipedia.org/wiki/Big_O_notation">
        http://en.wikipedia.org/wiki/Big_O_notation</a></li>
    </ul>
  </section>
</section>

<section>
  <h2>Importance of Algorithms</h2>
  <section>
    <h3>Introductory Example</h3>
    <p>Calculating the sum of all numbers
      <code>≤ n</code></p>
{% highlight extension %}
{% include unit_testing_path + 'before_refactoring.' + extension %}
{% endhighlight %}
    <p>What is wrong with the above naive implementation?</p>
    <p class="fragment">The asymptotic performance is $O(n)$</p>
  </section>

  <section>
    <h3>$O(1)$ - Constant Time Implementation</h3>
{% highlight extension %}
{% include unit_testing_path + 'after_refactoring_fixed.' + extension %}
{% endhighlight %}
<p>Which impact can $O(1)$ vs $O(n)$ have?</p>
<div class="fragment">
{% download data_path + 'sum_to.' + extension %}
{% highlight 'shell' %}
{% if language == 'Python' %}
time python sum_to.py linear
time python sum_to.py constant
{% else %}
{{ compiler }} sum_to.{{ extension }} -o sum_to
time ./sum_to n
time ./sum_to 1
{% endif %}
{% endhighlight %}
</div>
  </section>

  <section>
    <h3>Why Are Algorithms More Important Than Ever?</h3>
    <p>Despite computers being faster than ever, why are data structures and
      algorithms still
      important, even more so than in the past?
    </p>
    <p class="fragment">Because computers have to deal with larger amounts
      of data.
    </p>
    <p class="fragment">Computation time costs energy</p>
  </section>
</section>

<section>
  <h2>Big O notation</h2>

  <section>
  <ul>
    <li>Analyzes the worst case memory usage/ runtime of algorithms</li>
    <li>Big O provides an upper bound</li>
    <li>History
      <ul>
        <li>First introduced by number theorist Paul Bachmann in 1894</li>
        <li>Popularized in the work of number theorist Edmund Landau (Landau symbol)</li>
        <li>Donald Knuth popularized the notation in computer science</li>
      </ul>
    </li>
  </ul>
  </section>

  <section>
    <h3>Problem Classes</h3>
      <ul>
        <li>Constant</li>
        <li>Logarithmic</li>
        <li>Linear</li>
        <li>Loglinear</li>
        <li>Polynomial</li>
        <li>Exponential</li>{# TODO: np vs p #}
        <li>Factorial</li>
      </ul>
  </section>

  <section>
    <h3>Orders of Common Problem Classes</h3>
    <div style="text-align: left;">
      <p>$O(1) &lt; O(\log(n)) &lt; O(n) &lt; O(n \log(n)) &lt; O(n^2) &lt; O(n^k) \mbox{ for } k\gt 2$</p>
      <p class="fragment">$O(n^k) &lt; O(c^n) \mbox{ for } c\gt 1$</p>
      <p class="fragment">$O(c^n) &lt; O(n!)$</p>
    </div>
  </section>

  <section>
    <h3>Basic Rules</h3>
    <div style="text-align: left;">
      <p>$O(k*n) = O(n)$</p>
      <p class="fragment">$O(3n + 5n^2) = O(3n) + O(5n^2) = O(n) + O(n^2) = O(n^2)$</p>
      <p class="fragment">$O(n^2) + O(e^n) = O(e^n)$</p>
    </div>
  </section>

  <section>
    <h3>Counting Examples</h3>
    <p>How many steps are required, at most, for the following tasks?</p>
    <ul>
      <li>Calculate the sum of all elements in a collection</li>
      <li>Shake each other's hands</li>
      <li>Calculate the third power of each element in a $n \times n$
        square matrix</li>
      <li>Solve a TSP (brute force)</li>
      <li>Sort an array (see next slide)</li>
    </ul>
  </section>
</section>

<section>
  <h2>Sorting</h2>

  <section>
    <ul>
      <li>The fastest sorting algorithms are log-linear $O(n*\log n)$</li>
      <li>For pre-sorted data, performance may even be linear</li>
      <li>Multiple
        <a href="https://en.wikipedia.org/wiki/Sorting_algorithm#Comparison_of_algorithms">
        sorting algorithms</a> exist:
        <ul>
          <li><a href="https://en.wikipedia.org/wiki/Bubble_sort">Bubble sort</a>
            - $O(n^2)$</li>
          <li><a href="https://en.wikipedia.org/wiki/Merge_sort">Merge sort</a>
            - $O(n \log n)$</li>
          <li><a href="https://en.wikipedia.org/wiki/Quicksort">Quicksort</a>
            - $O(n \log n)$
          </li>
          <li><a href="https://en.wikipedia.org/wiki/Timsort">Timsort</a>
            - $O(n \log n)$
          </li>
          <li>...</li>
        </ul>
      </li>
    </ul>

    <aside class="notes">
      <p>Python uses Timsort</p>
    </aside>
  </section>

{% if language in ('C', 'C++') %}
  <section
    data-background-image="https://upload.wikimedia.org/wikipedia/commons/8/83/Bubblesort-edited-color.svg"
    data-background-size="contain"
    data-background-repeat="no-repeat"
    data-background-opacity="0.1">
    <h3>Bubble Sort</h3>
      <p>Repeatedly step through the list, compare adjacent elements and swaps them</p>
      <p> <a href="https://upload.wikimedia.org/wikipedia/commons/c/c8/Bubble-sort-example-300px.gif">Bubble sort animation</a></p>
    <p><a href="https://www.youtube.com/watch?v=semGJAJ7i74&list=PLOmdoKois7_FK-ySGwHBkltzB11snW7KQ&index=2">Bubble-sort with Hungarian ("Csángó") folk dance</a></p>
    <p>Exercise: implement the bubble sort algorthm<br />
      <code>void bsort(int* array, size_t cnt)</code></p>
    <p class="fragment">Although simple, it is not used in practice due to
      the slow $O(n^2)$ performance.
    </p>
  </section>

  <section>
    <h3>Use the standard library</h3>
    <p>In practice, don't implement your own search</p>
{% highlight extension + ' 7' %}
{% include source_path + 'sort.' + extension %}
{% endhighlight %}
{% download data_path + 'sort.' + extension %}
  </section>
{% endif %}

</section>


{#
https://www.youtube.com/watch?v=semGJAJ7i74&list=PLOmdoKois7_FK-ySGwHBkltzB11snW7KQ&index=2
 #}

{% include 'slides/closing.njk' %}

<section>
  <h2>Literature</h2>

  <div class="reference" id="Hetland:2010">
    <span class="authors">Magnus Lie Hetland</span>
    <span class="title">Python Algorithms: Mastering Basic Algorithms in the Python Language</span>
    <span class="pub_details">Apress (2010)</span>
  </div>

  <div class="reference" id="Weiss:2013">
    <span class="authors">Mark Allen Weiss</span>
    <span class="title">Data Structures and Algorithm Analysis in C++ </span>
    <span class="pub_details">Pearson (2013)</span>
  </div>
</section>

{% endblock %}{# presentation #}
