{#
Using extends instead of normal layout chaining is necessary here as
most of the content is shared between multiple unit test intros for different
programming languages with only a few blocks being distinct.
#}

{% extends "layouts/presentation.njk" %}

{% set title = "Programming in " + language %}
{% set subtitle = "Fast Track to " + language + " Syntax for Programmers" %}

{% set source_path = "source/" + extension + "/" %}
{% set data_path = "/data/" + extension + "/" %}

{% block presentation %}

<section>
  <h2>Compiling a Basic Program</h2>
{% highlight extension %}
{% include source_path + "hello." + extension %}
{% endhighlight %}
  <p>{% download data_path + "hello." + extension %}</p>
  <p>Compilation can be done with either <code>{{ compiler }}</code> or
    <code>{{ altCompiler }}</code> (part of the GNU Compiler Collection, GCC).</p>
{% highlight "bash" %}
{{ compiler }} $INFILE -o $PROGRAM_NAME
{{ altCompiler }} $INFILE -o $PROGRAM_NAME
{% endhighlight %}
</section>

<section>
  <h2>I/O</h2>
  <section>
    <h3>Writing to Standard Output</h3>
{% block cpp_io_streams %}{% endblock %}
{% highlight extension + " 0,4" %}
{% include source_path + "hello." + extension %}
{% endhighlight %}
{% include_code source_path + "hello." + extension %}
  </section>

  <section>
    <h3>Reading Input</h3>
{% highlight extension + " 0,6"%}
{% include source_path + "input." + extension %}
{% endhighlight %}
    <p>{% download data_path + "input." + extension %}</p>
  </section>
</section>

<section>
  <h2>Variables and Data Types</h2>
  <section>

{% block cpp_initialization %}{% endblock %}

{% highlight extension %}
{% include source_path + "data_types." + extension %}
{% endhighlight %}
{% include_code source_path + "data_types." + extension %}
  </section>

  <section>
    <h3>Block Scope</h3>
    <p>Identifiers are only valid in their defining block</p>
{% highlight extension %}
{% include source_path + "block_scope." + extension %}
{% endhighlight %}
    <p>{% download data_path + "block_scope." + extension %}</p>
  </section>

  {% block cpp_types %}{% endblock %}
</section>

<section>
  <h2>Conditional Execution</h2>

  <section>
    <h3><code>if</code> and <code>else</code></h3>
{% highlight extension %}
if (expression) {
  // block
} else if (expression) {
  // block
} else {
  block
}
{% endhighlight %}
    <div class="fragment">
    <p>The <a href="https://www.freecodecamp.org/news/c-ternary-operator/">ternary
      operator</a> provides a shorthand</p>
{% highlight extension %}
Type result = condition ? value_if_true : value_if_false;
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Example: <code>if</code> and <code>else</code></h3>
{% highlight "c 4,6,8" %}
{% include source_path + "if." + extension %}
{% endhighlight %}
{% include_code source_path + "if." + extension %}
  </section>

  <section>
    <h3><code>switch</code></h3>
    <p>The {{ language }} switch statement jumps to the block of code matching the value
      of an expression.<br />
      It executes one or multiple code blocks among many alternatives.</p>
{% highlight extension %}
switch (expression) ​{
    case constant1:
      // statements
      break;  // without break, execution of the following blocks continues
    case constant2:
      // statements
      break;
    // ...
    default:
      // statements
}
{% endhighlight %}
  </section>

  <section>
    <h3>Example: <code>switch</code></h3>
    <p></p>
{% highlight extension + " 2,3,5,6,9,12"%}
{% include source_path + "switch." + extension %}
{% endhighlight %}
{% include_code source_path + "switch." + extension %}
  </section>

</section>

<section>
  <h2><code>while</code> Loops</h2>

  <section>
{% highlight extension %}
while (expression) {
  // block
}
{% endhighlight %}
    <div class="fragment">
      <p><code>do ... while</code>: run code block at least once
        regardless of the expression</p>
{% highlight extension %}
do {
  // block
} while (expression);
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Example</h3>
{% highlight "c 4-7" %}
{% include source_path + "while." + extension %}
{% endhighlight %}
{% include_code source_path + "while." + extension %}
  </section>

  <section>
    <h3>Example: Execute a block of code at least once</h3>
    {% highlight extension + " 4,6" %}
    {% include source_path + "do." + extension %}
    {% endhighlight %}
    {% include_code source_path + "do." + extension %}
  </section>
</section>

<section>
  <h2><code>for</code> Loops</h2>

  <section>
{% highlight "c 4-7" %}
for (initialization; condition; update_statement) {
  // block
}
{% endhighlight %}
    <div class="fragment">
      <p><code>initialization</code> and <code>update_statement</code> are optional</p>
    </div>
    <div class="fragment">
      <p>Every <code>for</code> loop can be expressed as <code>while</code>
        loop</p>
{% highlight extension %}
// initialization
for (; condition; ) {
  // block
  // update_statement;
}

// initialization
while (condition) {
  // block
  // update_statement
}
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Example</h3>
{% highlight extension %}
{% include source_path + "for." + extension %}
{% endhighlight %}
{% include_code source_path + "for." + extension %}
  </section>

  {% block cpp_for %}
  {% endblock %}
</section>

<section>

  <section>
    <h2>Function Signature</h2>
    <p>aka. <strong>type signature</strong> or
      <strong>type annotation</strong></p>
    <p>Defines the data types of the parameters and return value</p>
    <div class="fragment">
      <p>For example, a function that returns the sum of two integers:</p>
{% highlight extension %}
(int)(int, int)
{% endhighlight %}
    </div>
  </section>

  <section>
    <h2>Function Declaration</h2>
    <p>aka. <strong>function prototype</strong> or
      <strong>function interface</strong></p>
      <ul>
        <li>Specifies the function name and type signature, but omits the
          body</li>
        <li>Required for using functions that are defined elsewhere</li>
        <li>Promise to the compiler that the function will exist when linking</li>
      </ul>
      <p></p>
    <div class="fragment">
      <p>For example, a function that returns the sum of two integers:</p>
{% highlight extension %}
int sum(int a, int b);
{% endhighlight %}
    </div>
    <div class="fragment">
      <p>The parameter names are optional:</p>
{% highlight extension %}
int sum(int, int);
{% endhighlight %}
    </div>
  </section>

  <section>
    <h2>Function Definition</h2>
    <ul>
      <li>Adds the function body to the declaration</li>
      <li>The signature and name must exactly match the declaration</li>
    </ul>
{% highlight extension %}
Type function_name(Type parameter1, Type parameter2, ...) { body }
{% endhighlight %}
    <div class="fragment">
      <p>For example, a function that returns the sum of two integers:</p>
{% highlight extension %}
int sum(int a, int b) {
  return a + b;
}
{% endhighlight %}
    </div>
  </section>

  <section>
    <h2>Pass by Value</h2>
    <p>By default, {{ language }} copies argument values to function
      parameters</p>
{% highlight extension + " 2,10" %}
{% include source_path + "by_value." + extension %}
{% endhighlight %}
{% include_code source_path + "by_value." + extension %}
  </section>

{% block cpp_functions %}{% endblock %}
</section>

{% block cpp_content %}{% endblock %}

{% include 'slides/closing.njk' %}
{% endblock %}
