The LaTeX source code for generating pdfs which are translated to the
svg files in this direction can be found in `/latex/` in the root of this
repository.
