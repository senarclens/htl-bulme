Note that the cpp files have been renamed from `c++` to `cpp` to facilitate
including them depending on the `{{ extension }}` variable.

text-x-c++hdr.svg => text-x-cpphdr.svg
text-x-c++src.svg => text-x-cppsrc.svg
