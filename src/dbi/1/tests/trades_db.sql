
drop database if exists trades;
create database trades;
use trades;

create table if not exists employees (
  employee_id       integer not null primary key auto_increment,
  first_name        varchar(30) not null,
  last_name         varchar(50) not null,
  salary            int,
  commission_rate   real
);
insert into employees
values (1,'Chris','Sanders',70000,3.0),
       (2,'Pat','Steward',55000,5.0),
       (3,'Sue','Hedborg',61000,4.0),
       (4,'Francois','Dupois',65000,3.0),
       (5,'Cat','Whitherspoon',75000,2.5);

create table if not exists customers (
  customer_id     integer not null primary key auto_increment,
  first_name      varchar(30) not null,
  last_name       varchar(50) not null,
  iban            varchar(30),
  country         varchar(50)
);
insert into customers
values (1,'Per','Kronberg','SE12 7463 1923 9451 2942', 'Sweden'),
       (2,'Leonie','Yvroud','FR94 2874 8475 1238 4845', 'France'),
       (3,'Derrek','Sonnamaker','AT32 0000 0012 3941 3944', 'USA');

create table if not exists orders (
  order_id     integer not null primary key auto_increment,
  filed        date,
  gross_price  real,
  customer_id  integer,
  employee_id  integer,
  foreign key(customer_id) references customers(customer_id),
  foreign key(employee_id) references employees(employee_id)
);
insert into orders
values (1,'2023-01-01',35000.0,3,5),
       (2,'2023-01-01', 8500.0,2,4),
       (3,'2023-01-02',15000.0,3,5),
       (4,'2023-01-03', 6800.0,1,2);
