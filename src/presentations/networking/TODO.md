# Physical Devices Presentation

## The Loopback Interface
* A logical interface that is internal to the host or router. It is not  
  assigned to a physical port and can never be connected to any other  
  device. It is considered a software interface that is automatically placed  
  in an “up” state, as long as a host is functioning.
* The loopback interface is useful in testing and managing hosts.
* It can be used for developing and testing applications that require a  
  network connection.
