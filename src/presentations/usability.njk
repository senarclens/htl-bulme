---
title: Usability and User Experience
subtitle: A High-Level Introduction
layout: layouts/presentation.njk
---


<section>
  <h2>What is Usability?</h2>

  <section>
    <figure>
      <blockquote>
        <p>The extent to which a product can be used by specified users to
          achieve specified goals with effectiveness, efficiency, and
          satisfaction in a specified context of use.</p>
      </blockquote>
      <figcaption><cite>
        <a href="https://www.iso.org/standard/63500.html">ISO 9241-11:2018</a>
        </cite>
      </figcaption>
    </figure>
  </section>

  <section>
    <dl>
      <dt>Ease of use</dt>
      <dd>Can users accomplish tasks easily?</dd>
      <dt>Learnability</dt>
      <dd>Can users pick up the software quickly?</dd>
      <dt>Efficiency</dt>
      <dd>Can users complete tasks quickly with minimal effort?</dd>
      <dt>Memorability</dt>
      <dd>How easily can users re-establish proficiency after periods
        of inactivity?</dd>
      <dt>Errors</dt>
      <dd>How does the software deal with (user) errors?</dd>
      <dd>How easily can they recover from errors?</dd>
      <dt>Satisfaction</dt>
      <dd>Are users happy with the software?</dd>
    </dl>
  </section>
</section>


<section>
  <h2>What is User Experience (UX)?</h2>

  <section>
    <figure>
      <blockquote>
        <p>A person's perceptions and responses that result from the use and/or
          anticipated use of a product, system or service.</p>
      </blockquote>
      <figcaption><cite>
        <a href="https://www.iso.org/standard/63500.html">ISO 9241-11:2018</a>
        </cite>
      </figcaption>
    </figure>
  </section>

  <section>
    <p>UX considers the entire user journey, not just usability.</p>
    <ul>
      <li>Includes user emotions, motivations, and overall satisfaction</li>
      <li>A good UX makes users feel confident, in control, and accomplished</li>
    </ul>
    <aside class="notes">
      <p>UX goes beyond usability.</p>
      <p>Tell students BMW feeling story (maybe watch
        <a href="https://www.youtube.com/watch?v=4knKphzeqA0"
           target="youtube-window">clip</a>)</p>
    </aside>
  </section>
</section>


<section>
  <section>
    <h2>Why is UI / UX important for Developers?</h2>
    <ul>
      <li>Developers play a key role in shaping the UI / UX</li>
      <li>Code decisions can directly impact how users interact with the
        software</li>
      <li>Understanding UI / UX principles helps developers make informed
        decisions</li>
      <li>Focusing on UI / UX helps build software users will love</li>
    </ul>
  </section>

  <section>
    <div style="display: grid; grid-auto-flow: column;">
    <figure>
      <img src="/images/programming/usability/remote_1.jpg"
           alt="remote control with 'pattern A'"
           style="max-height: 40%" />
      <figcaption>Remote Control A</figcaption>
    </figure>
    <figure>
      <img src="/images/programming/usability/remote_2.jpg"
           alt="remote control with 'pattern B'"
           style="max-height: 40%" />
      <figcaption>Remote Control B</figcaption>
    </figure>
    </div>
  </section>
</section>


<section>
  <h2>Designing for Users: Empathy is Key</h2>
  <ul>
    <li>Put yourselves in the user's shoes</li>
    <li>What are their goals and needs?</li>
    <li>What challenges might they face?</li>
    <li>Consider user personas to understand different user types</li>
  </ul>
  <aside class="notes">
    User personas are fictional representations of your target users,
    helping you design software that meets their needs.
  </aside>
</section>


<section>

  <section>
    <h2>Usability Testing</h2>
    <ul>
      <li>Observe real users interacting with your software</li>
      <li>Identify areas of confusion or difficulty</li>
      <li>Usability testing helps iterate and improve the design</li>
      <li>Early testing saves time and resources in the long run</li>
    </ul>
  </section>

  <section>
    <h2>Wireframes</h2>
    <p>Visual guide that represents the skeletal framework of a web page or
      application window</p>
    <ul>
      <li>Help visualize and communicate the design</li>
      <li>Allow testing the UI in early stages</li>
    </ul>
    <p class="fragment">Lo fidelity vs. high fidelity wireframes</p>

    <aside class="notes">
      <dl>
        <dt>Lo-fi</dt>
        <dd>usually grayscale, dummy content, no real images,
          not interactive</dd>
        <dt>Hi-fi</dt>
        <dd>(close to) real content, color, images, might be interactive</dd>
      </dl>
      <p>However, paper wireframes my be use "guided interactivity"</p>
    </aside>
  </section>

  <section>
    <h2>Wireframes, UI Mockups, Prototypes</h2>
    <ul>
      <li>Paper wireframes: drawn by hand using pencil and paper</li>
      <li class="fragment">Create mockups using open source tools like
        <a href="https://penpot.app/">Penpot</a> (SaaS),
        <a href="http://pencil.evolus.vn/">Pencil</a> or
        <a href="https://inkscape.org/">Inkscape</a></li>
      <li class="fragment">Mockups can be worked on using proprietary
        collaboration tools like
        <a href="https://www.figma.com/">Figma</a> (SaaS) or
        <a href="https://www.balsamiq.com/">Balsamiq</a> (SaaS)
      </li>
      <li class="fragment">If you have prototypes, use them for UI testing</li>
    </ul>
  </section>
</section>


<section>
  <h2>Accessibility</h2>
  <p>W3C's Web Accessibility Initiative <strong>WAI</strong></p>
  <p>Published the Accessible Rich Internet Applications Suite (ARIA)</p>
  <p>Consult <a href="https://www.w3.org/WAI/standards-guidelines/aria/">
    WAI-ARIA Overview</a> for more information</p>
</section>


<section>
  <h2>Consistency</h2>
  <p>Consistent UI patterns are most important</p>
  <p>Don't re-invent the wheel, use an existing set of guidelines</p>

  <ul>
    <li><a href="https://developer.android.com/design">
      Design for Android</a></li>
    <li><a href="https://developer.gnome.org/hig/">
      GNOME Human Interface Guidelines</a></li>
    <li><a href="https://developer.apple.com/design/human-interface-guidelines/">
      Human Interface Guidelines (Apple)</a></li>
    <li><a href="https://develop.kde.org/hig/">
      KDE Human Interface Guidelines</a></li>
    <li><a href="https://m3.material.io/">Material Design</a></li>
    <li><a href="https://learn.microsoft.com/en-us/windows/win32/appuistart/-user-interface-principles">
      User Interface Principles (Microsoft)</a></li>
  </ul>
</section>



{% include 'slides/closing.njk' %}

{#
Task: create simple UI draft for specific software and upload the design to
GH classroom
#}
<section>
  <h2>Further Reading</h2>
  <div class="reference" id="Senarclens:2024">
    <span class="authors">Gerald Senarclens de Grancy</span>
    <span class="title">Notes - Usability</span>
    <span class="pub_details">
      <a href="http://docs.find-santa.eu/notes/usability.html">
        http://docs.find-santa.eu/notes/usability.html</a></span>
  </div>
</section>
