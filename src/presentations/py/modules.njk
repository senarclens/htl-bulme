---
title: Programming in Python
subtitle: Modules and Packages
layout: layouts/presentation.njk
---

{% set source_path = "source/py/modules/" %}
{% set data_path = "/data/py/modules/" %}


<section>
  <h2>Modular Programming</h2>

  <section>
    <p>Emphasizes breaking down a program into smaller, manageable modules</p>
    <p>Modules should be self-contained (minimal dependencies on other
      modules)</p>
    <ul>
      <li>Functionality is grouped in interchangeable modules</li>
      <li>Modules are designed to have a specific responsibility within the
        system</li>
      <li>They provide an interface usable by other modules</li>
    </ul>

    <aside class="notes">
      <p>Modules allow code-reuse between programs like functions allow re-use
        within a program</p>
    </aside>
  </section>

  <section>
    <h3>Which Problems does Modular Programming Solve?</h3>

    <ul>
      <li>Modules also allow code-reuse between programs</li>
      <li>Independent modules are easier to test</li>
      <li>Maintenance is facilitated</li>
      <li>No copy and paste between programs</li>
      <li>Input/ UI, database and computations should not be mixed</li>
      <li>Facilitates specialization and division of labor</li>
    </ul>
  </section>

  <section>
    <h3>Which Problems does Modular Programming Cause?</h3>

    <ul>
      <li>Tooling has to support working with multiple files<br />
        eg. search and replace in multiple files</li>
      <li>Importing modules can be tricky using relative imports</li>
      <li>Splitting an application well requires experience</li>
    </ul>
  </section>
</section>

<section>

  <section>
    <h2>Python Modules</h2>
    <p>A module is a file containing Python definitions and statements.<br />
      Every <kbd>*.py</kbd> file is a
      <a href="http://docs.python.org/3/tutorial/modules.html">module</a></p>

    <ul class="fragment">
      <li>A program can contain many modules (<kbd>*.py</kbd> files)</li>
      <li>Modules (<kbd>*.py</kbd> files) can be imported or executed</li>
    </ul>
    <p class='fragment'>If a program consists of a single file,<br />
      that file is a module as well as a program</p>
    <p class='fragment'>Independent functionality should reside in different
      modules</p>
  </section>


  <section>
    <h2>Python Modules</h2>
    <p>Code in one module can be accessed from other modules</p>
{% highlight 'py' %}
import name  # this imports the code in `name.py`
from name import object  # this imports only one object defined in `name.py`
{% endhighlight %}


    <div class="fragment">
      <p>Modules provide namespaces (avoid name clashes)</p>
{% highlight 'py 5,6' %}
{% include source_path + 'name_spaces.py' %}
{% endhighlight %}
{% include_code source_path + 'name_spaces.py' %}
    </div>
  </section>

  <section>
    <h2>Python Modules - Example</h2>
    <p>A solver for quadratic equations that can be used by other modules</p>
{% highlight 'py' %}
{% include source_path + 'solver.py' %}
{% endhighlight %}
{% download data_path + 'solver.py' %}
  </section>

  <section>
    <h2>Python Modules - Example</h2>
    <p>A simple GUI using the above solver</p>
{% highlight 'py 2,5' %}
{% include source_path + 'ui.pyw' %}
{% endhighlight %}
{% download data_path + 'ui.pyw' %}
  </section>

  <section>
    <h2>Python Modules - Example</h2>
    <p>A simple CLI using the above solver</p>
{% highlight 'py 5,11' %}
{% include source_path + 'cli.py' %}
{% endhighlight %}
{% download data_path + 'cli.py' %}
  </section>
</section>


<section>
  <h2>How are modules found?</h2>
    <p>The interpreter first searches for built-in modules</p>

    <p class="fragment">When there is no built-in module matching the name,
      it searches for a matching filename in a list of directories:
      <code>
        <a href="http://docs.python.org/3/tutorial/modules.html#the-module-search-path">
          sys.path</a></code></p>

    <p class="fragment">To include additional directories in
      <code>sys.path</code> on every startup, add them to the
      <code>
        <a href="http://docs.python.org/3/using/cmdline.html#envvar-PYTHONPATH">
          PYTHONPATH</a></code> environment variable</p>

    <p class="fragment">To initialize <code>PYTHONPATH</code> automatically,
      modify your <kbd>~/.profile</kbd><br />
      (in most Linux distributions)</p>
</section>


<section>
  <h2><code>ifmain</code> Pattern</h2>

  <section>
    <p>The code in a module is executed whenever it is imported</p>
    <p>If different behavior is required when a program is executed,<br />
      developers can use the special <code>__name__</code> variable</p>
    <p>This variable is set by the Python interpreter:</p>
    <ul>
      <li>In imported code, <code>__name__</code>, is set to the name of
        the imported module</li>
      <li>In executed scripts <code>__name__</code>, is set to
        <code>'__main__'</code></li>
    </ul>
  </section>

  <section>
    <p>The <code>ifmain</code> pattern allows to distinguish if code is
      imported or executed</p>
{% highlight "py" %}
{% include source_path + 'ifmain.py' %}
{% endhighlight %}
{% download data_path + 'ifmain.py' %}
  </section>

  <section>
    <h3>Status Codes</h3>
    <p>Programs indicate to the OS whether or not their
      execution was successful</p>
    <p>In Python, this is done explicitly by calling
      <code>sys.exit(.)</code></p>
    <p>Status code 0 indicates success, all other codes indicate failure</p>
    <p>This is usually combined with the <code>ifmain</code>
      pattern</p>
{% highlight "py" %}
{% include source_path + 'ifmain_status.py' %}
{% endhighlight %}
{% download data_path + 'ifmain_status.py' %}
</section>


<section>
  <p>Code that should only run when a script is executed is best
    placed in the <code>main()</code> function
  </p>

  <p>The following example shows the complete <code>ifmain</code> pattern</p>
{% highlight "py" %}
{% include source_path + 'ifmain_complete.py' %}
{% endhighlight %}
{% download data_path + 'ifmain_complete.py' %}
  </section>

  <section>
    <p>Below is a possible template for executable scripts</p>
{% highlight "py" %}
{% include source_path + 'template.py' %}
{% endhighlight %}
{% download data_path + 'template.py' %}
  </section>
</section>


<section>
  <section>
    <h2>Packages</h2>
    <p>Python programs and libraries are installed from
      <a href="http://docs.python.org/3/tutorial/modules.html#packages">
        packages</a></p>
    <p>Packages are a means of publishing related modules as program
      or library</p>
    <dl class="fragment">
      <dt>Python Package Index (<strong>PyPI</strong>)</dt>
      <dd>official third-party software repository for Python</dd>
      <dd><a href="https://pypi.python.org/">pypi.python.org</a></dd>
    </dl>
  </section>

  <section>
    <h2>Installing Packages</h2>
    <ul>
      <li>Pip Installs Python (<kbd>pip</kbd>)
        <ul>
          <li>Standard Python package manager</li>
          <li>Downloads packages and their dependencies from PyPI</li>
          <li>Formerly, <kbd>easy_install</kbd> was the main installer</li>
          <li><kbd>pip install ${PACKAGE_NAME}</kbd></li>
        </ul>
      </li>
      <li class="fragment">Alternatively, use your distribution's installer
        <ul>
          <li>Debian-based: <kbd>sudo apt install python3-${PACKAGE_NAME}</kbd></li>
          <li>emerge (Gentoo), pacman (Arch), pamac (Manjaro), yum, ...</li>
        </ul>
      </li>
    </ul>
    <p class="fragment">To avoid collisions between the distribution's package
      manager and PyPI, always use a
      <a href="http://docs.find-santa.eu/programming/py/virtualenv.html">
        virtual environment</a> when installing from PyPI
    </p>
  </section>

  {# TODO: create proper and up to date description for creating own packages
           however, this is likely best done in a separate presentation

   <section>
    <h3>Creating and Publishing Your Own Packages</h3>
    <li>Every code directory needs a
      <a href="https://docs.python.org/3/tutorial/modules.html#packages"><code>__init__.py</code></a></li>
    <li><code>setup.py</code> file is at the heart of a Python project</li>
    <li>Upload your own Python packages to <a href="http://pypi.python.org/">PyPI</a></li>
    <li><a href="http://guide.python-distribute.org/">How To Package Your Python Code</a></li>
  </section> #}
</section>


{% include 'slides/closing.njk' %}


<section>
  <h2>Further Reading</h2>
  <div class="reference" id="Henney:2010">
    <span class="authors">Kevlin Henney (edt.)</span>
    <span class="title">97 Things Every Programmer Should Know</span>
    <span class="pub_details">O'Reilly (2010)</span>
  </div>
  <div class="reference" id="pilgrim:2009">
    <span class="authors">Mark Pilgrim</span>
    <span class="title"><a href="http://getpython3.com/diveintopython3/">Dive Into Python 3</a> (2nd edition)</span>
    <span class="pub_details">Apress (October 23, 2009)</span>
  </div>
  <div class="reference" id="python:2012">
    <span class="authors">Python Software Foundation</span>
    <span class="title"><a href="http://docs.python.org/3/">Python Documentation</a></span>
    <span class="pub_details">http://docs.python.org/3/</span>
  </div>
  <!-- list of all links for printouts -->
  <div id="link-list"></div>
</section>

