// definition of constants for C presentations
const interpreter = 'python';

module.exports = {
  altCompiler: interpreter,
  comment: '#',
  compiler: interpreter,
  end: '',
  extension: 'py',
  header: '',
  language: 'Python',
};
