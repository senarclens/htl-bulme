---
title: Programming in Python
subtitle: Built-in Container Data Structures
layout: layouts/presentation.njk
---

{% set source_path = "source/py/containers/" %}
{% set data_path = "/data/py/containers/" %}

<section>
  <h2>Terminology</h2>
  <dl>
    <dt><a href="http://en.wikipedia.org/wiki/Software_bug">Bug</a></dt>
    <div class="fragment">
      <dt>Data</dt>
      <dd>We usually think about lots of data => we need containers of data</dd>
      <dd>Containers are objects that contain other objects</dd>
      <dd>Python sequence types are containers</dd>
    </div>
    <div class="fragment">
      <dt>Alias</dt>
      <dd>Alternative way of referring to an object</dd>
    </div>
  </dl>
</section>


<section class="center">
  <blockquote style="width: 100%;">
    <p>What's in a name? That which we call a rose
      by any other name would smell as sweet.</p>
    <footer>– <cite>William Shakespeare</cite></footer>
  </blockquote>
</section>


<section>
  <h2>Python Lists</h2>

  <section>
    <p>Aka. array, ArrayList, vector, ...</p>
    <p>Mutable (can be changed in-place)</p>
    <p>Contiguous memory - not a "linked list"</p>
    <p>Sequence type</p>
    <ul class="fragment">
      <li>Support indexing and slicing:
        <code>[index], [start:stop:step]</code></li>
      <li>Aware of their length: <code>len(.)</code></li>
      <li>Membership testing: <code>in</code></li>
      <li>Can be iterated over: <code>for ... in</code></li>
    </ul>

    <aside class="notes">
      <p>Which sequence type do we already know? -- string (immutable)</p>
    </aside>
  </section>

  <section>
    <p>Much more than a C-style array</p>
    <ul>
      <li>Built-in functionality</li>
      <li><strong>In-place</strong> methods</li>
      <li>Can be sorted</li>
    </ul>
    <p class="fragment">Optimized for usability, not speed</p>
    <div class="fragment">
{% highlight "py" %}
a_list = [3, 54, 2]
empty = []  # or `list()`
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Working with Lists</h3>
{% highlight "py" %}
{% include source_path + "lists.py" %}
{% endhighlight %}
{% include_code source_path + "lists.py" %}
  </section>

  <section>
    <h3>Performance</h3>
    <ul>
      <li>Don't worry about performance until it becomes a problem</li>
      <li>But: understand the way lists work (contiguous memory)</li>
      <li>Many inserts at beginning vs. reverse -> append -> reverse</li>
    </ul>
{% highlight "py" %}
ITERATIONS = 1000000

l = [7, 9, 3]  # initial content
for i in range(ITERATIONS):
    l.insert(0, 0)  # slow

l = [7, 9, 3]  # initial content
l.reverse()
for i in range(ITERATIONS):
    l.append(0)  # fast
l.reverse()
{% endhighlight %}
  </section>

  <section>
    <h3>Assignment</h3>
    <p>Python assigns object references</p>
{% highlight "py" %}
{% include source_path + "reference.py" %}
{% endhighlight %}
{% include_code source_path + "reference.py" %}

    <div class="fragment">
      <p>Shallow copy: <code>list(.)</code>, <code>[:]</code> (slicing),
        <code>l.copy()</code></p>
{% highlight "py" %}
{% include source_path + "list_copy.py" %}
{% endhighlight %}
{% include_code source_path + "list_copy.py" %}
    </div>
  </section>

  <section>
    <p>
      <a href="https://docs.python.org/3/tutorial/datastructures.html#more-on-lists">
        The Python Tutorial - More on Lists</a></p>
    <p>
      <a href="https://www.youtube.com/watch?v=CsbtYedEsFE">
        Python 3 Video Tutorial - Lists</a>
    </p>
  </section>
</section>


<section>

  <section>
    <h2>Tuples</h2>
    <p>Tuples are comparable to lists, but immutable</p>
    <p>Sequence type</p>

{% highlight "py" %}
a_tuple = (3, 54, 2)
empty = ()  # or `tuple()`
t2 = (3, )  # creating a tuple with a single element requires trailing comma
{% endhighlight %}
  </section>

  <section>
    <h2>Named Tuples</h2>
    <p>Allow to name their values</p>
    <p>Values can also be accessed with dot notation</p>
{% highlight "py 3" %}
{% include source_path + "named_tuple.py" %}
{% endhighlight %}
{% include_code source_path + "named_tuple.py" %}
  </section>

  <section>
    <h2>Tuples</h2>
    <p>
      <a href="https://docs.python.org/3/tutorial/datastructures.html#tuples-and-sequences">
        The Python Tutorial - Tuples and Sequences</a></p>
    <p>
      <a href="https://www.youtube.com/watch?v=6HDMPz867cY">
        Python 3 Video Tutorial - Tuples</a>
    </p>
  </section>
</section>


<section>
  <h2>Multiple Assignment</h2>

  <section>
    <p>Pythonic way to work with "more than one return value"</p>

    <p>Multiple values can be assigned at once</p>
{% highlight "py 3" %}
{% include source_path + "ma_retrieve.py" %}
{% endhighlight %}
{% include_code source_path + "ma_retrieve.py" %}
  </section>

  <section>

    <p>Seamless way to return "multiple values"</p>
{% highlight "py 4" %}
{% include source_path + "ma_return.py" %}
{% endhighlight %}
{% include_code source_path + "ma_return.py" %}

    <p class="fragment">Also useful for swapping elements:
      <code>a, b = b, a</code></p>
  </section>
</section>


<section>

  <section>
    <h2>Dictionaries</h2>
    <p>Aka. associative array or map</p>
    <p>Collection of key-value pairs with unique keys</p>
    <p>Similar to lists, but "keys" are used as indices</p>
    <p>Key order is guaranteed to be insertion order (since Python 3.7)</p>
    <p>Implemented as
      <a href="https://en.wikipedia.org/wiki/Hash_table">hash table</a>
      (aka. hash map)</p>
{% highlight "py" %}
d = {"key1": "value1", "key2": "value2"}
empty = {}  # or `dict()`
{% endhighlight %}
  </section>

  <section
    data-background="https://upload.wikimedia.org/wikipedia/commons/d/d0/Hash_table_5_0_1_1_1_1_1_LL.svg"
    data-background-size="contain">
  </section>

  <section>
    <h2>Dictionaries</h2>
    <h3>Example: Populate a <code>dict</code></h3>
{% highlight "py" %}
{% include source_path + "populate_dict.py" %}
{% endhighlight %}
{% include_code source_path + "populate_dict.py" %}
  </section>

  <section>
    <h2>Dictionaries</h2>
    <h3>Example: Iterate over a <code>dict</code></h3>
{% highlight "py 5" %}
{% include source_path + "iterate_dict.py" %}
{% endhighlight %}
{% include_code source_path + "iterate_dict.py" %}
  </section>

  <section>
    <h2>Dictionaries</h2>
    <h3>Example: Iterate over Sorted Keys</h3>
{% highlight "py 5" %}
{% include source_path + "iterate_sorted.py" %}
{% endhighlight %}
{% include_code source_path + "iterate_sorted.py" %}
    <div class="fragment">
      <p>
        <a href="https://docs.python.org/3/tutorial/datastructures.html#dictionaries">
          The Python Tutorial - Dictionaries</a></p>
      <p><a href="http://youtu.be/HqldXgH_RcE">
        Python 3 Video Tutorial - Dictionaries</a></p>
    </div>
  </section>
</section>

<section>
  <h2>Sets</h2>

  <section>
    <p>Collection of <strong>unique</strong> elements</p>
    {#<ul>
      <li>Search, removal, and insertion: logarithmic $O(\log{n})$</li>
    </ul>#}
    <p>Sets are useful wherever mathematical sets are useful</p>
{% highlight "py" %}
s1 = {1, 4, 5, 3, 1, 3, 5, 4, 3, 1, 3, 4}  # only 4 (distinct) elements
empty = set()  # {} would be an empty dictionary
{% endhighlight %}
  </section>

  <section>
    <h3>Theory - Intersection</h3>
    <p><strong>Intersection</strong> of two sets $A$ and $B$,
      denoted by $A \cap B$ is the set containing all elements of $A$
      that also belong to $B$</p>
      <img src="https://upload.wikimedia.org/wikipedia/commons/9/99/Venn0001.svg"
           alt="[set intersection diagram]">
  </section>

  <section>
    <h3>Theory - Union</h3>
    <p><strong>Union</strong> (denoted by $\cup$) of a collection of sets
        is the set of all elements in the collection</p>
      <img src="https://upload.wikimedia.org/wikipedia/commons/3/30/Venn0111.svg"
           alt="[set union diagram]">
  </section>

  <section>
    <h3>Example: Working with Sets</h3>
{% highlight "py" %}
{% include source_path + "sets.py" %}
{% endhighlight %}
{% include_code source_path + "sets.py" %}

  <div class="fragment">
    <p><a href="https://docs.python.org/3/tutorial/datastructures.html#sets">
      The Python Tutorial - Sets</a></p>
    <p><a href="http://youtu.be/HlG1it321vU">
      Python 3 Video Tutorial - Sets</a></p>
  </div>
  </section>
</section>


<section>
  <h2>Matrices</h2>

  <section>
    <p>Rectangular array of entries, arranged in rows and columns</p>
    <p>Python does not offer a built-in type to efficiently deal with
      matrices</p>
    <p>NumPy is the most popular third party package adding support for
      multi-dimensional arrays
    </p>
    <div class="fragment">
{% highlight "shell" %}
sudo apt install python3-numpy  # global installation
pip install numpy  # in virtual environment
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Example: Matrix Operations</h3>
{% highlight "py 2" %}
{% include source_path + "np_matrix.py" %}
{% endhighlight %}
{% download data_path + "np_matrix.py" %}
  </section>

  <section>
    <p>NumPy is a library important enough to be dealt with
      in a designated session
    </p>
    <p><a href="https://numpy.org/doc/stable/user/absolute_beginners.html">
      NumPy: the absolute basics for beginners</a></p>
    <p><a href="http://youtu.be/T587ZFDcWNo">
      Python 3 Video Tutorial - 2D Data</a></p>
  </section>
</section>

{% include 'slides/closing.njk' %}
