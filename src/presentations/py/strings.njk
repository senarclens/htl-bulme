---
title: Programming in Python
subtitle: Strings
layout: layouts/presentation.njk
---

<section>
  <h2>Terminology</h2>

  <section>
    <ul>
      <li>Datatype</li>
      <li class="fragment">Char array vs. string</li>
      <li class="fragment">Immutable
        <ul>
          <li>No assignment to elements allowed</li>
          <li>Methods return new objects</li>
        </ul>
      </li>
      <li class="fragment">Function signature</li>
    </ul>
  </section>

  <section>
    <h3>Sequence Data Types</h3>
    <p>Index, membership, size, slicing and iteration</p>
    <ul>
      <li class="fragment">Can be described by a start point and an
        endpoint</li>
      <li class="fragment">Support the membership operator (<code>in</code>)
        and the size function (<code>len()</code>)</li>
      <li class="fragment">Elements can be selected with their index in square
        brackets</li>
      <li class="fragment">Indices start at 0</li>
      <li class="fragment">Slicing: extract part of a sequence as a new
        sequence</li>
      <li class="fragment">Sequences are iterable</li>
    </ul>
  </section>

  <section>
    <h3>Library</h3>
    <ul>
      <li>Collection of pre-built code we can use</li>
      <li class="fragment">Using them avoids duplicate work</li>
      <li class="fragment">They are usually designed by professionals</li>
      <li class="fragment">Standard library vs. third party libraries</li>
    </ul>
    <p class="fragment">
      <a href="https://docs.python.org/3/library/functions.html">
        Built-in functions</a> (BIFs) are always available</p>

    <blockquote class="fragment"
                style="margin-left: 0; margin-right: 0; width: 100%;">
      <p>...we again and again avoid doing complicated work and
      instead find simpler solutions - often relying on library facilities.
      That's the essence of programming: the continuing search for
      simplicity.</p>
      <footer>– <cite>Bjarne Stroustrup</cite></footer>
    </blockquote>
  </section>
</section>

<section class="center"
         data-background-gradient="radial-gradient(#88bbaa, #17b2c3)">
  <h1><a href="https://summerofcode.withgoogle.com/">
    Google Summer of Code</a></h1>
</section>


<section>
  <h2>Python Strings</h2>

  <section>
    <p><code>'text'</code>, <code>""</code>, <code>''''''</code>,
      <code>""""""</code></p>
    <div class="fragment">
{% highlight "py" %}
s.lower() -> str
s.split([sep[, maxsplit]]) -> list of strings
s.upper() -> str
...
{% endhighlight %}
    </div>
  </section>

  <section>
  <h3>Example - String Operations</h3>
{% highlight "py 3,5,9" %}
{% include "source/py/strings.py" %}
{% endhighlight %}
{% include_code "source/py/strings.py" %}
  </section>

  <section>
    <h3>Substrings</h3>
    <ul>
      <li>The first element of any sequence has the index 0</li>
      <li>A second index points to the element after a slice</li>
      <li>Start and end are available by skipping an index</li>
    </ul>
  </section>

  <section>
    <h3>Example - Index and Slicing</h3>
{% highlight "py" %}
{% include "source/py/substrings.py" %}
{% endhighlight %}
{% include_code "source/py/substrings.py" %}
  </section>

  <section>
    <h3>f-Strings</h3>
    <p> Defined in <a href="https://peps.python.org/pep-0498/">
      PEP 498 - Literal String Interpolation</a></p>
    <p><code>f'text {var} text'</code>, <code>f""</code>, <code>f''''''</code>,
      <code>f""""""</code></p>
{% highlight "py" %}
{% include "source/py/f-strings.py" %}
{% endhighlight %}
{% include_code "source/py/f-strings.py" %}
    <p><a href="https://docs.python.org/3/library/string.html#formatspec">
      Format Specification Mini-Language</a></p>
  </section>

  <section>
    <p><a href="https://www.youtube.com/watch?v=qiwCUl-9NIo">
      Python 3 Video Tutorial - Strings</a></p>
    <p><a href="http://youtu.be/oMFI_bIjBnk">
      Python 3 Video Tutorial - String Formatting</a></p>
  </section>
</section>


<section>
  <h2>For Loops</h2>

  <section>
    <p>Iterate over the elements of a sequence</p>
    <p>Each time the body of a for loop runs, a variable is set to the next element of a sequence</p>
    <div class="fragment">
      <h4>Example</h4>
{% highlight "py" %}
{% include "source/py/for.py" %}
{% endhighlight %}
{% include_code "source/py/for.py" %}
    </div>
  </section>

  <section>
    <h3><code>range(.)</code></h3>
{% highlight "py" %}
range(stop) -> range object
range(start, stop[, step]) -> range object
{% endhighlight %}

        {# <li><code>range(5)</code> means [0:5) which provides the
          values 0, 1, 2, 3 and 4</li> #}

    <div class="fragment">
      <h4>Example</h4>
{% highlight "py" %}
{% include "source/py/range.py" %}
{% endhighlight %}
{% include_code "source/py/range.py" %}
    </div>
  </section>

  <section>
    <p><a href="http://youtu.be/rK-LGeTLFK4">
      Python 3 Video Tutorial - For Loops</a></p>
  </section>
</section>

<section>
  <h2>Reading Files</h2>

  <ol>
    <li>Open the file with <code>open(.)</code></li>
    <li>Read the file with <code>read()</code>, <code>readline()</code>,
      <code>readlines()</code> or iterate over its content</li>
    <li>Close the file with <code>close()</code> or
      implicitly (end of <code>with</code>)</li>
  </ol>

  <div class="fragment">
  <p>The recommended way to read files is</p>
{% highlight "py" %}
with open(filename, encoding='utf-8') as f:
    for line in f:
        print(line, end="")
{% endhighlight %}

    <p>Note that <code>filename</code> must contain a
      the name of a readable file (string)</p>
  </div>
  <p class="fragment"><a href="http://youtu.be/ms098rsJhrk">
    Python 3 Video Tutorial - Reading and Writing Files</a></p>
</section>

<section>
  <h2>Summary</h2>
  <ul>
    <li>Elements of a sequence are referenced by indices</li>
    <li>Where do indices start? Why?</li>
    <li>Which data types do we know by now?</li>
    <li>Built-in functions
      <ul>
        <li><code>enumerate(.)</code>, <code>open(.)</code>,
          <code>range(.)</code></li>
      </ul>
    </li>
    <li>Standard library functions
      <ul>
        <li>s.lower(), s.upper(), ...</li>
      </ul>
    </li>
  </ul>
</section>


{% include 'slides/closing.njk' %}
