---
title: Programming in Python
subtitle: 'Functional Programming'
layout: layouts/presentation.njk
---

{% set source_path = "source/py/fp/" %}
{% set data_path = "/data/py/fp/" %}


<section>
  <h2>Terminology</h2>

  <h3>Data Persistence</h3>
  <ul>
    <li>Files - easiest way to store objects is using <code>pickle</code></li>
    <li>Database - organized collection of data</li>
  </ul>

  <div class="fragment">
    <h3>PEP *</h3>
    <ul>
      <li>Python Enhancement Proposals</li>
      <li>Primary documentation regarding the design and development of the Python programming language</li>
      <li><a href="http://www.python.org/dev/peps/">http://www.python.org/dev/peps/</a></li>
    </ul>
  </div>
</section>

<section>

  <section>
    <h2>Functional-Style Programming</h2>
    <ul>
      <li>Programming paradigm focusing on evaluating mathematical
        functions</li>
      <li>Based on combining functions that don't modify their
        arguments</li>
      <li>Functions don't use or change the program's state</li>
      <li>Allows directly expressing mathematical concepts</li>
      <li>Functions solely use <code>return</code> values to communicate their
        results</li>
      <li>In theory, functions are completely independent of each other</li>
      <li>Prominent functional languages include Elixir, Erlang, Haskell and
        SML</li>
      <li>Used mainly in academia, but also in industrial projects</li>
    </ul>

    <aside class="notes">
    </aside>
  </section>


  <section>
    <h2>Concepts of functional programming</h2>
      <dl>
        <dt>Pure functions</dt>
        <dt>First-class and higher-order functions</dt>
        <dd>Functions are
          <a href="http://en.wikipedia.org/wiki/First-class_citizen">
            first-class citizens</a></dd>
        <dt>Anonymous (lambda) functions</dt>
        <dt>Recursion</dt>
        <dd>
          Function definition depends on
          (a simpler version of) itself
        </dd>
        <dt><a href="http://www.pythontutor.com/visualize.html#code=def+foo(y)%3A%0A++++def+bar(x)%3A%0A++++++++return+x+%2B+y%0A++++return+bar%0A%0Ab+%3D+foo(1)%0Ab(2)&amp;mode=display&amp;cumulative=false&amp;heapPrimitives=false&amp;drawParentPointers=false&amp;textReferences=false&amp;showOnlyOutputs=false&amp;py=3&amp;curInstr=0">
          Closures</a></dt>
        <dd>
          an abstraction binding a function to its scope<br />
          a closure is a record storing a function together with an environment
        </dd>
        <dt>Currying</dt>
        <dd>Partially applying arguments to a function</dd>
        <dt>...</dt>
      </dl>

    <aside class="notes">
      <dl>
        <dt>Pure functions</dt>
        <dd><code>return</code> values are identical for identical arguments
          (no variation with local static variables, non-local variables,
           mutable reference arguments or input streams)</dd>
        <dd>no side effects (no mutation of local static variables, non-local
          variables, mutable reference arguments or input/output streams)</dd>
      </dl>
    </aside>
  </section>
</section>


<section>
  <h2>Writing Files</h2>

  <section>
    <h3>Purpose</h3>
    <ul>
      <li>Write the results of your computations to files</li>
      <li>Keep a log of errors</li>
      <li>Store intermediate results</li>
      <li>...</li>
    </ul>
  </section>

  <section>
    <p>Works analog to reading files</p>
{% highlight "py" %}
with open(filename, 'w', encoding='utf8') as f:
    f.write(.)
    f.writelines(.)
{% endhighlight %}

    <div class="fragment">
      <p>You may also redirect <code>print</code></p>
{% highlight "py" %}
with open(filename, 'w', encoding='utf8') as f:
    print(., file=f)
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Example</h3>
{% highlight "py 5,6,9,10" %}
{% include source_path + 'write_file.py' %}
{% endhighlight %}
{% download data_path + 'write_file.py' %}
  </section>

  <section>
    <h3>Example: Append</h3>
    <p>Writing to the end of an existing file</p>
{% highlight "py 5" %}
{% include source_path + 'append_file.py' %}
{% endhighlight %}
{% download data_path + 'append_file.py' %}
  </section>

  <section>
    <h3>Example: Writing Binary Files</h3>
    <p>Danger: binary files are not human readable</p>
    <p>Storing objects is easiest with <code>pickle</code></p>
{% highlight "py 0,4,5" %}
import pickle
result = [[1, 3, 2],
          [4, 5, 6],
          [9, 8, 7]]
with open('test.dump', 'wb') as f:  # wb or bw => write binary
    pickle.dump(result, f)
{% endhighlight %}

    <div class="fragment">
      <p>Reading "pickled" objects is straightforward</p>
{% highlight "py 1,2" %}
import pickle
with open('test.dump', 'rb') as f:  # rb or br => read binary
    result = pickle.load(f)
{% endhighlight %}
    </div>
  </section>

</section>


<section>
  <h2>Anonymous Functions</h2>

  <section>
    <p>Aka function constant, function literal, or <code>lambda</code>
      function</p>
    <ul>
      <li>Function defined without being bound to an identifier</li>
      <li>Essential part of the functional programming paradigm</li>
      <li>Emphasizes the application of functions</li>
    </ul>

    <aside class="notes">
      <ul>
        <li>Also available in other general purpose languages like Python and
          JavaScript</li>
        <li>Added to C++ in 2011 (C++11)</li>
      </ul>
    </aside>
  </section>

  <section>
    <h3>Purpose and Applications</h3>
    <ul>
      <li>Arguments to a higher-order functions</li>
      <li>Special scoping rules</li>
      <li><a href="http://en.wikipedia.org/wiki/Anonymous_function#Sorting">
        Sorting</a>, callbacks, closures, ...</li>
    </ul>

    <div class="fragment">
      <h3>Syntax</h3>
{% highlight "py" %}
lambda arg [, arg2, ...]: expression
{% endhighlight %}
      <p>The result of <code>expression</code> is implicitly returned</p>
    </div>
  </section>

  <section>
    <h3>Example: Assign Lambda to Identifier</h3>
    <p>It is possible to assign lambdas to identifiers</p>
{% highlight "py" %}
{% include source_path + 'lambda_example.py' %}
{% endhighlight %}
{% include_code source_path + 'lambda_example.py' %}
  </section>

  <section>
    <h3>Example: Sorting a List of Lists</h3>
    <p>Default sorting criterion: each sublist's first element</p>
{% highlight "py" %}
{% include source_path + 'sort_matrix.py' %}
{% endhighlight %}
{% include_code source_path + 'sort_matrix.py' %}

    <div class="fragment">
      <p>If versatile control is needed, lambdas can be used</p>
{% highlight "py 3" %}
{% include source_path + 'sort_matrix_lambda.py' %}
{% endhighlight %}
{% include_code source_path + 'sort_matrix_lambda.py' %}
    </div>
  </section>

  <section>
    <h3>Example: Sorting a List of Strings</h3>
      <p>By default, strings are sorted by the unicode code points of their
        characters
      </p>
      <p>Using lambdas allows more natural sorting</p>
{% highlight "py 4" %}
{% include source_path + 'sort_strings.py' %}
{% endhighlight %}
{% include_code source_path + 'sort_strings.py' %}

    <aside class="notes">
      <p>Mention and show ASCII table as simple entry to character encodings

        ideally add link to OWN (!) ASCII table
      </p>
    </aside>
  </section>
</section>


<section>
  <h2>Recursion</h2>

  <section>
    <p>Function calling itself</p>
    <ul>
      <li>The definition of a function depends on a simpler version of
        itself</li>
      <li>Facilitates implementing certain algorithms</li>
      <li>Allows focusing on the next step - not the whole problem at once</li>
      <li>Mathematical concepts can be expressed with straightforward,
        easy to read code
        <ul>
          <li>Recursive implementation can be re-implemented with loops</li>
          <li>Loops tend to be more efficient but harder to code</li>
          <li>Watch out for the limit on the depth of recursion (avoid stack overflows)</li>
        </ul>
      </li>
    </ul>
  </section>

  <section>
    <h3>Applications</h3>
    <ul>
      <li>Low level: working with data structures such as lists and trees</li>
      <li>High level: algorithms using such data structures
        (eg. graph algorithms)</li>
      <li>Divide and conquer serves as top-down approach to problem solving</li>
      <li>Implementation of solutions to certain mathematical problems</li>
      <li>...</li>
    </ul>
  </section>

  <section>
    <h3>Example:
      <a href="http://en.wikipedia.org/wiki/Factorial">Factorial</a>
      of a Non-negative Integer</h3>
    <p>$\mbox{factorial(n) = }\begin{cases}1 &amp; \mbox{if n }\in \mbox{ set([0, 1])}\\
        \mbox{n * factorial(n - 1)} &amp; \mbox{otherwise} \end{cases}$</p>
    <div class="fragment">
{% highlight "py 7-9" %}
{% include source_path + 'factorial.py' %}
{% endhighlight %}
{% include_code source_path + 'factorial.py' %}
    </div>
  </section>

  <section>
    <h3>Example: Calculate the n<sup>th</sup>
      <a href="http://en.wikipedia.org/wiki/Fibonacci_number">
        Fibonacci number</a>
    </h3>
    <p>$\mbox{fib(n) = }\begin{cases}\mbox{n} &amp; \mbox{if n }\in \mbox{ set([0, 1])}\\
        \mbox{fib(n - 1) + fib(n - 2)} &amp; \mbox{otherwise} \end{cases}$</p>
    <div class="fragment">
{% highlight "py 6-8" %}
{% include source_path + 'fibonacci.py' %}
{% endhighlight %}
{% include_code source_path + 'fibonacci.py' %}
    </div>
  </section>
</section>


<section>

  <section>
    <h2><code>map</code>, <code>filter</code></h2>
    <p>Examples of higher-order functions</p>
    <p>For efficiency, both return iterators yielding values one at a time</p>
    <dl>
      <dt><code>map(function, *iterables)</code></dt>
      <dd>compute the function using arguments from the iterables</dd>
      <dt><code>filter(function, iterable)</code></dt>
      <dd>yield items of iterable for which function(item) is true</dd>
    </dl>
  </section>

  <section>
    <h2>Example: <code>map</code></h2>
    <p>Calculate the squares of a sequence</p>
{% highlight "py" %}
{% include source_path + 'map.py' %}
{% endhighlight %}
{% include_code source_path + 'map.py' %}
  </section>

  <section>
    <h2>Example: <code>filter</code></h2>
    <p>Filter odd numbers</p>
{% highlight "py" %}
{% include source_path + 'filter.py' %}
{% endhighlight %}
{% include_code source_path + 'filter.py' %}
  </section>

</section>


<section>
  <section>
    <h2>Reduce, All, Any</h2>
    <p><dl>
      <dt><code>functools.reduce(function, iterable[, initial])</code></dt>
      <dd>apply a function with two arguments cumulatively to iterable</dd>
      <div class="fragment">
        <dt><code>all(iterable) -> bool</code></dt>
        <dd>return <code>True</code> if <code>bool(x)</code> is
          <code>True</code> for all values in the iterable</dd>
        <dd>return <code>True</code> if the iterable is empty</dd>
        <dd>input can be created with <code>map</code> using a boolean valued
          function</dd>
      </div>
      <div class="fragment">
        <dt><code>any(iterable) -> bool</code></dt>
        <dd>return <code>True</code> if <code>bool(x)</code> is
          <code>True</code> for any value in the iterable</dd>
        <dd>return <code>False</code> if the iterable is empty</dd>
      </div>
    </dl></p>

    </section>

    <section>
      <h2>Example: Calculate the Product of a Sequence</h2>
{% highlight "py" %}
{% include source_path + 'reduce.py' %}
{% endhighlight %}
{% include_code source_path + 'reduce.py' %}

    <div class="fragment">
      <p><code>operator</code> module provides functions for Python's
        operators<br />
        E.g. <code>operator.mul</code> corresponds to
        <code>lambda x, y: x * y</code>
      </p>
{% highlight "py" %}
{% include source_path + 'reduce_mul.py' %}
{% endhighlight %}
{% include_code source_path + 'reduce_mul.py' %}
    </div>
  </section>

  <section>
    <h2>Example: Are There Only Odd Numbers?</h2>
{% highlight "py 1,4" %}
{% include source_path + 'all_odd.py' %}
{% endhighlight %}
{% include_code source_path + 'all_odd.py' %}
  </section>

  <section>
    <h2>Example: Did All Students pass?</h2>
{% highlight "py 9,12" %}
{% include source_path + 'any_negative.py' %}
{% endhighlight %}
{% include_code source_path + 'any_negative.py' %}
  </section>
</section>


<section>

  <section>
    <h2>Comprehensions</h2>

    <ul>
      <li>Particularly useful when doing a transformation on entire
        collections</li>
      <li>Less code to maintain than with a regular loop</li>
      <li>More verbose than using map and filter</li>
      <li>Comprehensions are available for lists, dictionaries and sets</li>
      <li>Generator expressions use lazy evaluation and are more memory
        efficient</li>
    </ul>

  </section>

  <section>
    <h2>Example: List Comprehension</h2>
{% highlight "py" %}
{% include source_path + 'list_comprehension.py' %}
{% endhighlight %}
{% include_code source_path + 'list_comprehension.py' %}
  </section>

  <section>
    <h2>Example: Dictionary Comprehension</h2>
{% highlight "py" %}
{% include source_path + 'dict_comprehension.py' %}
{% endhighlight %}
{% include_code source_path + 'dict_comprehension.py' %}
  </section>

  <section>
    <h2>Example: Generator Expression</h2>
    <p>Use parenthesis instead of square brackets</p>
    <p>Unlike list comprehensions, values are computed 'on demand'</p>
{% highlight "py" %}
{% include source_path + 'generator_expression.py' %}
{% endhighlight %}
{% include_code source_path + 'generator_expression.py' %}
  </section>

</section>


<section>
  <h2>Summary</h2>
  <p>Functional programming emphasizes pure functions and avoids side
    effects</p>
  <ul>
    <li>Recursion is easier to code but runs slower than equivalent loops</li>
    <li>Anonymous functions facilitate, for example, sorting</li>
    <li>If you are interested in functional programming...
      <ol>
        <li>Get a designated textbook!</li>
        <li>Learn a functional programming language like
          <a href="https://elixir-lang.org/">Elixir</a>.</li>
      </ol>
    </li>
    <li>Topics not covered include
      <ul>
        <li><a href="http://en.wikipedia.org/wiki/Partial_function_application">
          Partial function application</a>
          (e.g. <code>functools.partial(.)</code>)</li>
        <li><a href="http://en.wikipedia.org/wiki/Closure_%28computer_science%29">
          Closures</a></li>
        <li>...</li>
      </ul>
    </li>
  </ul>
</section>


{% include 'slides/closing.njk' %}


<section>
  <h2>Further Reading</h2>

  <div class="reference" id="python:2024"">
    <span class="authors">A. M. Kuchling</span>
    <span class="title"><a href="http://docs.python.org/3/howto/functional.html">Functional Programming HOWTO</a></span>
    <span class="pub_details">http://docs.python.org/3/howto/functional.html</span>
  </div>
  <div class="reference" id="pilgrim:2009">
    <span class="authors">Mark Pilgrim</span>
    <span class="title"><a href="http://getpython3.com/diveintopython3/">Dive Into Python 3</a> (2nd edition)</span>
    <span class="pub_details">Apress (October 23, 2009)</span>
  </div>
  <div class="reference" id="python:2012">
    <span class="authors">Python Software Foundation</span>
    <span class="title"><a href="http://docs.python.org/3/">Python Documentation</a></span>
    <span class="pub_details">http://docs.python.org/3/</span>
  </div>
  <!-- list of all links for printouts -->
  <div id="link-list"></div>
</section>

