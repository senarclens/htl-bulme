// definition of constants for C presentations

module.exports = {
  altCompiler: 'gcc',
  compiler: 'clang',
  comment: '//',
  end: ';',
  extension: 'c',
  header: 'hpp',
  language: 'C',
};
