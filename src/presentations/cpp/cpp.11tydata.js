const language = 'C++';
const extension = 'cpp';
const comment = '//';
const compiler = 'clang++';
const end = ';';
const altCompiler = 'g++';
const styleguide = 'https://google.github.io/styleguide/cppguide.html#';
const guidelines = 'https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#';

// alternative for the future is to export a function whose return values will
// be used
// module.exports = () => {
//   return {
//     language,
//     foo: 'bar',
//   };
// };

// an alternative to const definitions above is to define the values directly
// as with the `header`
module.exports = {
  altCompiler,
  comment,
  compiler,
  coreCpp: 'https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#',
  end,
  guidelines,
  extension,
  header: 'hpp',
  googleCpp: 'https://google.github.io/styleguide/cppguide.html#',
  language,
  styleguide,
};
