---
title: Programming in C++
subtitle: Exceptions
layout: layouts/presentation.njk

requires: classes, inheritance
CoreCpp: https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#
GoogleCpp: https://google.github.io/styleguide/cppguide.html#
---

<section>
  <section>
    <h2>Exceptions</h2>
    <p>Allow to pass errors back to the caller without returning error codes</p>
    <p>They permit to decouple error handling from the regular control flow</p>
    <p>Exceptions deal with anticipated, but uncommon cases, eg.:</p>
    <ul>
      <li>Files that are not readable/ writable</li>
      <li>A required server being unavailable</li>
      <li>...</li>
    </ul>

    <p class="fragment">
      If something should never happen, use assertions instead
    </p>

    <aside class="notes">
      <p>The basic idea is that when an error occurs, an exception is
        "thrown".</p>

      <p>Errors should be dealt with where they occur if possible.</p>

      <p>Assertions detect programming errors during development.
        If an assertion fails, <code>std::abort</code> is called.</p>
    </aside>
  </section>

  <section>
    <h2>Exceptions</h2>
    <p>If a function cannot reasonably deal with an error</p>
    <ul>
      <li>It's caller has a chance to catch the error</li>
      <li>It's caller’s caller has a chance to catch the error</li>
      <li>...</li>
      <li><code>main(.)</code> has a chance to catch the error</li>
      <li>If an error is not caught, the program terminates
        (<code>std::terminate()</code>) with an
        exception error</li>
    </ul>

    <aside class="notes">
      <p>When std::terminate() it is implementation-defined whether stack
        unwinding is done. If the stack is not unwound, variables will not be
        destroyed, and any cleanup upon destruction won't happen!</p>
    </aside>
  </section>

  <section>
    <h2>Purpose of Exceptions</h2>
    <p>When recovering from an error is not straight forward in the current code
      unit</p>
    <ul>
      <li>Exceptions allow splitting up error detection and error handling</li>
      <li>Can avoid fatal errors (eg. <code>std::exit(ERROR_CODE)</code> is
        not user friendly)</li>
      <li>Returning error codes might obscure a function's actual intent<br />
        <code>return</code> does not work in constructors</li>
      <li>Exceptions avoid having to constantly check for errors</li>
      <li>They hence can facilitate understanding the intent of the code</li>
    </ul>

    <aside class="notes">
      <p><code>std::abort()</code> does not call global object destructors</p>
      <p><code>std::terminate()</code> is called by the C++ runtime in case of
        uncaught exceptions</p>
    </aside>
  </section>

  <section>
    <h2>Disadvantages of C++ Exceptions</h2>
    <dl>
      <dt>Performance Overhead</dt>
      <dd>Expensive in terms of runtime, especially when used frequently</dd>

      <dt>Hard to Get Right</dt>
      <dd>Exceptions make the control flow harder to evaluate</dd>
      <dd>Exception safety requires both RAII and specific coding practices</dd>
      <dd>Careless throwning or recovering from them may lead to leaks</dd>

      <dt>Potential for Misuse</dt>
      <dd>Use exceptions only for error handling!</dd>
      <dd>Eg. invalid user input should not cause exceptions to be thrown</dd>
    </dl>
    <p>See eg. <a href="{{ styleguide }}Exceptions">
          Google C++ Style Guide</a></p>

    <p class="fragment">Avoid unhandled exception at all costs</p>

    <aside class="notes">
      <p>Some of the disadvantages are specific to C++</p>
      <p>Google prohibits using C++ exceptions</p>
      <p>Golang does not offer exceptions at all</p>
    </aside>
  </section>
</section>


<section>
  <h2>Syntax</h2>

  <section>
    <h3>Throwing an Exception</h3>
    <p>If a function cannot deal with a problem, it may use the
      <code>throw</code> statement</p>
    <p>This is also commonly called "raising an exception"</p>
{% highlight "cpp" %}
throw std::runtime_error("Description of what went wrong.");
{% endhighlight %}

    <div class="fragment"><p>You may throw</p>
      <ul>
        <li>numeric or <code>string</code> literals</li>
        <li>instances of any class (possibly derived form
          <code>std::exception</code>)</li>
        <li>any of the standard exceptions
          except <code>std::exception</code><br />
          (see <a href="https://en.cppreference.com/w/cpp/error/exception">
            cppreference</a> for a list of available exceptions)</li>
      </ul>
    </div>

    <aside class="notes">
      <p>C++ can also throw literal numbers or text</p>
      <p>However, a good practice is to throw something derived from
        <code>std::exception</code>, eg. <code>std::runtime_error</code></p>
    </aside>
  </section>

  <section>
  <h3>Catching an Exception</h3>
  <p>In a function directly or indirectly calling another function that uses
    <code>throw</code></p>
{% highlight "cpp" %}
try {  // observe if any of the following statements throw an exception
  // Code that might throw an exception
} catch ( const MostSpecificException& e ) {  // eg. std::out_of_range
  // handle custom exception
} catch ( const LessSpecificException& e ) {  // eg. std::logic_error
  // handle custom exception
} catch ( const std::exception& e ) {
  // handle all other standard exceptions
} catch ( ... ) {
  std::cerr << e.what() << std::endl;
  // Handle everything else (catch-all handler)
}
{% endhighlight %}

    <aside class="notes">
      <p>Exceptions should be caught by const reference to avoid expensive
        copying.</p>
    </aside>
  </section>

  <section>
    <h3>Defining C++ Exceptions</h3>
    <p>Deriving from <code>std::exception</code> ensures a consistent
      interface</p>
{% highlight "cpp" %}
class YourException : public std::exception {
public:
  const char* what() const noexcept {
    return "My exception occurred!";
  }
};
{% endhighlight %}

    <aside class="notes">
      <p>Technically, your exceptions don't have to inherit from
        <code>std::exception</code> and don't have to implement
        <code>what()</code>.
      </p>
      <p>
        To stick to the convention has the advantage of consistency.
        However, the cost is having to return a <code>const char*</code>
        and hence losing flexibility to solely use modern C++.</p>
    </aside>
  </section>

  <section>
    <h3><code>noexcept</code> specifier</h3>

    <p>Functions are either non-throwing or potentially throwing</p>
    <p>Functions can be defined as non-throwing using the <code>noexcept</code>
      specifier</p>
{% highlight "cpp" %}
void f() noexcept;
void f() noexcept {
  // ...
}
{% endhighlight %}
    <p class="fragment"><code>noexcept</code> is <strong>not enforced</strong>
      at compile time</p>

  <aside class="notes">
    <p><code>noexcept</code> does not prevent the function from throwing or
      calling other functions that are potentially throwing. This is ok as
      long as it catches and handles those exceptions.</p>

    <p>The compiler still allows a <code>noexcept</code> function not to
      handle an exception, but <code>std::terminate()</code> is called
      at runtime in such cases.</p>
  </aside>
  </section>
</section>

<section>
  <h2>Related Guidelines</h2>

  <section>
    <h3><code>noexcept</code> specifier</h3>

    <p>
      <dl>
        <dt><a href="{{ guidelines }}e12-use-noexcept-when-exiting-a-function-because-of-a-throw-is-impossible-or-unacceptable">
          E.12</a></dt>
        <dd>
          Use <code>noexcept</code> when exiting a function because of
          throw is impossible [...]
        </dd>
        <dt><a href="{{ guidelines }}f6-if-your-function-must-not-throw-declare-it-noexcept">
          F.6</a></dt>
        <dd>
          If your function must not throw, declare it <code>noexcept</code>
        </dd>
        <dt><a href="{{ guidelines }}Rc-eq">
          C.86</a></dt>
        <dd>
          Make <code>==</code> symmetric with respect to operand types and
          <code>noexcept</code>
        </dd>
      </dl>
    </p>
{# consider removing the following example ... don't know why I added it #}
{% highlight "cpp" %}
struct S {
  uint64_t class;
  int64_t number;
};

bool operator==(const S& a, const S& b) noexcept {
  return a.class == b.class && a.number == b.number;
}
{% endhighlight %}

    <aside class="notes">
      <p>Use <code>noexcept</code> sparingly (only on functions that must not
        throw or fail)</p>
      <p>Adding <code>noexcept</code> later is safe while removing it may break
        existing code relying on the <code>noexcept</code> interface</p>
    </aside>
  </section>
</section>

<section>
  <h2>Example: <code>main(.)</code> Catching All Exceptions</h2>

  <section>
  {% highlight "cpp 11-14" %}
  {% include "source/cpp/exceptions/catch_all.cpp" %}
  {% endhighlight %}
  {% include_code "source/cpp/exceptions/catch_all.cpp" %}
  </section>

  <section>
    <p>To help your imagination ...</p>
  {% highlight "cpp 5,18-21" %}
  {% include "source/cpp/exceptions/catch_all_complete.cpp" %}
  {% endhighlight %}
  {% include_code "source/cpp/exceptions/catch_all_complete.cpp" %}
  </section>

  <section>
    <p>Instead, with uncaught exceptions (which should really be avoided)
      ...</p>
  {% highlight "cpp 10,16" %}
  {% include "source/cpp/exceptions/catch_all_missing.cpp" %}
  {% endhighlight %}
  {% include_code "source/cpp/exceptions/catch_all_missing.cpp" %}
  </section>
</section>

<section>
  <h2>Example: File I/O Error</h2>
{% highlight "cpp 7,16,17" %}
{% include "source/cpp/exceptions/file_io_error.cpp" %}
{% endhighlight %}
{% download "/data/cpp/exceptions/file_io_error.cpp" %}
</section>

<section>
  <h2>Example: Working with Designated Exception</h2>
{% highlight "cpp 4,6-9,17,28,51" %}
{% include "source/cpp/exceptions/invalid_circle.cpp" %}
{% endhighlight %}
{% download "/data/cpp/exceptions/invalid_circle.cpp" %}

  <aside class="notes">
    Note that <code>.c_str()</code> only stays valid while the exception
    object is in scope.
  </aside>
</section>


{% include 'slides/closing.njk' %}
