#!/usr/bin/env python

"""
Download data from OWID and store it in a local sqlite3 database.

The data can then be used for further analysis.
Use `NULL` for missing values.
Only insert values for a selection of countries.
"""

import codecs
import csv
import sqlite3
import urllib.request


URL = ('https://raw.githubusercontent.com/owid/covid-19-data/master/'
       'public/data/excess_mortality/excess_mortality.csv')
COUNTRIES = ('Austria', 'Sweden')

con = sqlite3.connect('mortality_database.db')
cur = con.cursor()
cur.execute('''
create table if not exists countries (
    country_id integer primary key,
    name text not null
);
''')
cur.execute('''
create table if not exists mortality (
    mortality_id integer primary key,
    country_id integer,
    week_start_iso_date text,
    week_excess real,
    cumulative_excess real,
    week_excess_per_million real,
    cumulative_excess_per_million real,
    foreign key (country_id) references countries(country_id)
);
''')

country_id = 0
country = ''
with urllib.request.urlopen(URL) as stream:
    reader = csv.DictReader(codecs.iterdecode(stream, 'utf-8'))
    for row in reader:
        if row['location'] not in COUNTRIES:
            continue
        if country != row['location']:
            country = row['location']
            country_id += 1
            cur.execute(f'''
INSERT INTO countries (country_id, name) VALUES ({country_id}, '{country}');
''')
        week_excess = row['excess_proj_all_ages'] or 'NULL'
        cumulative_excess = row['cum_excess_proj_all_ages'] or 'NULL'
        week_excess_per_million = (
            row['excess_per_million_proj_all_ages'] or 'NULL')
        cumulative_excess_per_million = (
            row['cum_excess_per_million_proj_all_ages'] or 'NULL')
        query = f'''
INSERT INTO mortality (country_id, week_start_iso_date,
                       week_excess, cumulative_excess,
                       week_excess_per_million, cumulative_excess_per_million)
VALUES ({country_id}, '{row['date']}',
        {week_excess}, {cumulative_excess},
        {week_excess_per_million}, {cumulative_excess_per_million});
'''
        cur.execute(query)

con.commit()
cur.close()
con.close()

