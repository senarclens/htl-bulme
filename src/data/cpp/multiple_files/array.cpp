#include "array.hpp"  // local includes use quotation marks

#include <cstddef>

namespace at {

/*
 * Return an array range of integers from 0 to dimension - 1.
 * The array must be freed after use.
 */
int* arange(size_t dimension) {
  int* array = new int[dimension * sizeof(int)];
  for (size_t i = 0; i < dimension; ++i) {
    array[i] = (int) i;
  }
  return array;
}

}  // namespace at
