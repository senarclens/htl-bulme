#ifndef UI_HPP
#define UI_HPP

#include <cstddef>

namespace at {

size_t get_dimension(int argc, char** argv);
void print_array(int array[], size_t dimension);

}  // namespace at

#endif  // UI_HPP
