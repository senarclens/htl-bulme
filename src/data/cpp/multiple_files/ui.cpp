#include "ui.hpp"  // local includes use quotation marks

#include <iostream>
#include <cstdlib>

#include "array.hpp"

using std::cerr, std::cin, std::cout, std::endl;
using std::size_t;


namespace at {

size_t get_dimension(int argc, char** argv) {
  if (argc == 1) {
    cerr << "Error: no arguments given." << endl;
  } else if (argc == 2) {
    return atol(argv[1]);
  } else {
    cerr << "Error: too many arguments given." << endl;
  }
  cerr << "Usage: `array_tool dimension`" << endl;
  cerr << "    dimension is an integer >= 1" << endl;
  std::exit(EXIT_FAILURE);
}

void print_array(int array[], size_t dimension) {
  cout << "[";
  for (size_t i = 0; i < dimension - 1; ++i) {
    cout << array[i] << ", ";
  }
  cout << array[dimension - 1] << ']' << endl;
}

}  // namespace at
