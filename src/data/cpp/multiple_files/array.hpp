#ifndef ARRAY_HPP
#define ARRAY_HPP

#include <cstddef>

namespace at {

int* arange(size_t dimension);

} // namespace at

#endif  // ARRAY_HPP
