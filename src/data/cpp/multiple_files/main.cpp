#include <cstddef>

#include "ui.hpp"  // local includes use quotation marks
#include "array.hpp"

int main(int argc, char** argv) {
  size_t dimension = at::get_dimension(argc, argv);
  int* array = at::arange(dimension);
  at::print_array(array, dimension);
  delete[] array;
  return 0;
}
