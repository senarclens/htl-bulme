#include <iostream>

int main() {
  int number = 0;
  std::cout << "enter an integer (0 <= number <= 9999): ";
  std::cin >> number;
  std::cout << "You entered " << number << std::endl;
  return 0;
}
