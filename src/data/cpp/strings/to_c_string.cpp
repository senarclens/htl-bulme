#include <cstring>  // provides C string functions like `strcpy(.)`
#include <iostream>
#include <string>


int main() {
  std::string s("C++ strings are character arrays in the background");
  char c_string[100];  // standard C++ does not allow `s.length() + 1` here
  strcpy(c_string, s.c_str());  // `c_str()` returns a `const char*`
  std::cout << "C string: " << c_string << std::endl;
  return 0;
}
