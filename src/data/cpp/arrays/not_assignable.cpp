#include <cstdio>

int main() {
  int first[] = {1, 2, 3};
  int second[] = {4, 5, 6};
  first = second;  // compiler error: array type is not assignable
  first = {7, 8, 9};  // also a compiler error
  printf("{ %d, %d, %d }", first[0], first[1], first[2]);
  return 0;
}
