#include <iostream>

int main() {
  unsigned int numbers[3];  // number of elements known at compile time
  numbers[0] = 15;
  numbers[2] = 5;
  numbers[1] = 10;
  std::cout << numbers[0] << " " << numbers[1] << " " << numbers[2] << "\n";
  return 0;
}
