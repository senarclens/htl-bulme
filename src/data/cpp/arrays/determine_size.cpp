#include <iostream>
using std::cout, std::endl;
int main() {
  int values[] = {-5, 39, 13, -21};  // compiler determines number of elements
  cout << "used memory: " << sizeof(values) << endl;
  cout << "number of elements: " << sizeof(values) / sizeof(int) << endl;
  return 0;
}
