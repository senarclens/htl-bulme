#include <iostream>
using std::cout, std::endl;

enum Color { RED, GREEN, BLUE, };
enum Orientation { NORTH, EAST, SOUTH, WEST, };

int main() {
  if (RED == NORTH) {  // valid (at least, decent compilers warn about this)
    cout << "what a mess" << endl;
  }
  return 0;
}
