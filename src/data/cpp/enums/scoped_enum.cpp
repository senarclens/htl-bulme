#include <iostream>
#include <utility>
using std::cout, std::endl;

enum class Orientation {
  NORTH,
  EAST,
  SOUTH,
  WEST,
};

std::ostream& operator<<(std::ostream& os, Orientation o) {
  switch (o) {
  case Orientation::NORTH:
    os << "north";
    break;
  case Orientation::EAST:
    os << "east";
    break;
  case Orientation::SOUTH:
    os << "south";
    break;
  case Orientation::WEST:
    os << "west";
    break;
  }
  return os;
}

int main() {
  Orientation o = Orientation::EAST;  // user controls movement of character
  std::cout << "The hero faces towards " << o << "." << std::endl;
  std::cout << std::to_underlying(o) << std::endl;  // since C++23
  return 0;
}
