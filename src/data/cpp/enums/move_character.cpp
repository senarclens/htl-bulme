#include <iostream>
using std::cout, std::endl;

enum Orientation {
  NORTH,
  EAST,
  SOUTH,
  WEST,
};

int main() {
  Orientation heading = EAST;  // user controls movement of character
  switch (heading) {
  case NORTH:
    cout << "Hero moves north...\n";
    break;
  case EAST:
    cout << "Hero moves east...\n";
    break;
  case SOUTH:
    cout << "Hero moves south...\n";
    break;
  case WEST:
    cout << "Hero moves west...\n";
    break;
  }
  return 0;
}
