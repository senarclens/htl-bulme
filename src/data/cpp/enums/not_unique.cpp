#include <iostream>
using std::cout, std::endl;

enum Color {
  RED = 5,
  GREEN,
  BLUE = 5,  // outch
};

int main() {
  if (RED == BLUE) {  // valid (an no compiler warning about this)
    cout << "what a mess" << endl;
  }
  return 0;
}
