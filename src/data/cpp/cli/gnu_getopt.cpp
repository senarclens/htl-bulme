#include <getopt.h>  // getopt_long
#include <iostream>
#include <string>
using std::cout, std::endl;
int main(int argc, char** argv) {
  int c;
  bool has_a = false, has_b = false;  // flags
  int is_superuser = 0;
  int verbosity_level = 0;
  std::string c_arg, d_arg;  // option_arguments
  static struct option long_options[] = {
  /* NAME       HAS_ARG            FLAG  VAL (SHORTNAME) */
    {"add",     required_argument, NULL, 0},
    {"append",  no_argument,       NULL, 1000},
    {"delete",  no_argument,       NULL, 0},
    {"verbose", no_argument,       NULL, 'v'},
    {"create",  required_argument, NULL, 'c'},
    {"file",    required_argument, NULL, 0},
    {"config",  optional_argument, NULL, 1001},
    {"su",      optional_argument, &is_superuser,  1},
    {NULL,      0,                 NULL, 0}
  };
  int option_index = 0;
  while ((c = getopt_long(argc, argv, "abc:d:vz",
          long_options, &option_index)) != -1) {
    switch (c) {
    case 0:
      cout << "option " << long_options[option_index].name;
      if (optarg) {
        cout << " with arg " << optarg;
      }
      cout << endl;
      break;
    case 1000:  // append
      cout << "`append` option was given" << endl;
      break;
    case 1001:  // config
      cout << "`config` option was given";
      if (optarg) {  // --config=configfilename
        cout << " with arg " << optarg;
      }
      cout << endl;
      break;
    case 'a':
      has_a = true;
      break;
    case 'b':
      has_b = true;
      break;
    case 'c':
      c_arg = optarg;
      break;
    case 'd':
      d_arg = optarg;
      break;
    case 'v':
      ++verbosity_level;
      break;
    case '?':  // getopt_long returns '?' on error (unknown option)
      cout << "dealing with invalid option (see getopt_long output)" << endl;
      break;
    default:  // "catches" option 'z' which isn't dealt with above
      cout << "getopt returned character code 0" << std::oct << c << " ??\n";
    }
  }
  if (optind < argc) {
    cout << "non-option ARGV-elements: ";
    while (optind < argc) {
      cout << argv[optind++] << " ";
    }
    cout << endl;
  }
  if (is_superuser) {
    cout << "Superuser mode is on" << endl;
  }
  cout << "a: " << has_a << ", b: " << has_b << ", c_arg: " << c_arg
       << ", d_arg: " << d_arg << ", verbosity-level: " << verbosity_level
       << endl;

  return 0;
}
