#include <iostream>
#include <string>
#include <unistd.h>  /* for getopt */
using std::cout, std::endl, std::oct, std::string;
int main (int argc, char **argv) {
  int c{0};
  bool aopt = false, bopt = false;  // flags
  string copt, dopt;  // option arguments
  while ((c = getopt(argc, argv, "abc:d:")) != -1) {
    switch (c) {
    case 'a':
      aopt = true;
      break;
    case 'b':
      bopt = true;
      break;
    case 'c':
      copt = optarg;
      break;
    case 'd':
      dopt = optarg;
      break;
    default:
      cout << "?? getopt returned character code 0" << oct << c << " ??\n";
    }
  }
  if (optind < argc) {
    cout << "non-option ARGV-elements: ";
    while (optind < argc) {
      cout << argv[optind++] << " ";
    }
    cout << endl;
  }
  cout << "aopt: " << aopt << ", bopt: " << bopt << ", copt: " << copt
       << ", dopt: " << dopt << endl;
  return 0;
}
