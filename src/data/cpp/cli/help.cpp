#include <iostream>
#include <cstdlib>
#include <string>
using std::cout, std::endl, std::string;
int main(int argc, char **argv) {
  for (int i{1}; i < argc; ++i) {
    string arg{argv[i]};
    if (arg == "-h" or arg == "--help") {
      cout << "Usage: a.out [OPTION]... " << endl;
      cout << "-h, --help display this help and exit" << endl;
      std::exit(0);
    }
  }
  cout << "To solve the world's problems, use the correct arguments" << endl;
  return 0;
}
