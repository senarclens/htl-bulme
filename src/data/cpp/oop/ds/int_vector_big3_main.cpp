#include "int_vector_big3.hpp"
#include <iostream>

int main(void) {
  ds::IntVector v1;
  v1.push_back(5);
  v1.push_back(1);
  v1.push_back(10);
  ds::IntVector v2{v1};  // copy constructor
  v2.pop_back();
  ds::IntVector v3;
  v3 = v2;  // copy assignment
  v3.push_back(42);

  std::cout << "v1: " << v1 << std::endl;
  std::cout << "v2: " << v2 << std::endl;
  std::cout << "v3: " << v3 << std::endl;
}

