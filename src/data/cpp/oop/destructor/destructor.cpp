#include <iostream>
#include <string>

using std::cout;

class Parent {
public:
  explicit Parent(std::string name) : name_{name} {
    cout << "Parent " << name_ << " is constructed\n";
  }
  // always use virtual destructors to make sure children on the heap
  // are properly destroyed (polymorphism)
  virtual ~Parent() {
    cout << "Parent " << name_ << " is destroyed\n";
  }
  std::string name() const { return name_; }
private:
  std::string name_;
};

class ChildWithDestructor : public Parent {
public:
  // parent constructor has to be called explicitly
  explicit ChildWithDestructor(std::string name) : Parent(name) {
    cout << "Child with destructor " << name << " is constructed\n";
  }
  ~ChildWithDestructor() {
    cout << "Child with destructor " << name() << " is properly destroyed\n";
    cout << "It's parent class' destructor is automatically called\n";
  }
};

class Child : public Parent {
public:
  explicit Child(std::string name) : Parent(name) {
    cout << "Child " << name << " is constructed\n";
  }
};

int main() {
  Parent p{"A"};
  Child c1{"B"};
  ChildWithDestructor c2{"C"};

  Parent* c3 = new Child("D");
  delete c3;
  Parent* c4 = new ChildWithDestructor("E");
  delete c4;
  return 0;
}
