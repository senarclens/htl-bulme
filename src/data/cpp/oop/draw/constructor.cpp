#include <iostream>
struct Point {
  int x = 0;  // default values are allowed since C++11
  int y = 0;
  Point(int x, int y) : x(x), y(y) {}  // constructor with initializer list
  Point() : Point(0, 0) {}  // Point() delegates to Point(0, 0)
  void print() {
    std::cout << "x: " << x << ", y: " << y << std::endl;
  }
};
int main() {
  Point p1;  // same as `Point p1 = Point()`
  Point p2{-1, 2}; // use constructor with arguments
  Point p3(5, 3);  // alternative to `Point p3 = Point(5, 3)`
  p2.print();
  return 0;
}
