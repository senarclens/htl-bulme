#include <iostream>

struct Point {
  int x = 0;
  int y = 0;
  Point(int x, int y) : x(x), y(y) {}  // constructor with initializer list
  Point() = default;  // use compiler generated default constructor
  void print() {
    std::cout << "x: " << x << ", y: " << y << std::endl;
  }
};
int main() {
  Point* p = new Point(5, 3);  // create object on the free store
  p->print();
  delete p;  // free the reserved memory
  return 0;
}
