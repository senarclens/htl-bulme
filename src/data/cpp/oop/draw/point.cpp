#include "point.hpp"
#include <iostream>

namespace draw {

// Point::Point(int x, int y) : x(x), y(y) {};  // struct scope would be needed

void Point::print() {  // method within struct scope
  std::cout << "x: " << x << ", y: " << y << std::endl;
}

}  // namespace draw
