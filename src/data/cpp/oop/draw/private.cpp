#include <iostream>
struct Point {
  int y() { return y_; }
  void y(int y) { y_ = y; }
  int x = 0;  // default value is possible as of C++11
private:
  int y_ = 0;
};
int main() {
  Point p;
  std::cout << "x: " << p.x << std::endl;
  p.x = 5;
  std::cout << "x: " << p.x << std::endl;
  std::cout << "y: " << p.y() << std::endl;
  p.y(3);  // not allowed to access p.y_ directly
  std::cout << "y: " << p.y() << std::endl;
  return 0;
}
