#ifndef POINT_HPP
#define POINT_HPP

namespace draw {
struct Point {
  int x;
  int y;
  Point(int x, int y) : x(x), y(y) {}; // constructor with initializer list
  Point() = default;  // we don't define anything - no need to do this in .cpp
  void print();  // method is declared, not defined
};
}

#endif
