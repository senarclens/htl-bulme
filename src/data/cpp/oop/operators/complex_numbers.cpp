#include "complex_numbers.hpp"

#include <iostream>
// ...

Complex Complex::operator+(const Complex& c) const {
  return Complex{real_ + c.real(), imag_ + c.imag()};
}

Complex operator+(double d, const Complex& c) {
  return c + d;  // returns result of `operator+(double d)` defined in class
}

std::ostream& operator<<(std::ostream& out, const Complex& c) {
  out << c.real() << (c.imag() >= 0 ? "+" : "") << c.imag() << + "j";
  return out;
}
