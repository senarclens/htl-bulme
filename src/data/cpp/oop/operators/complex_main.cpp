#include "complex_numbers.hpp"

#include <iostream>

int main() {
  Complex c1{5, 3};
  Complex c2{3, -2};
  std::cout << "c1: " << c1 << ", c2: " << c2 << std::endl;
  std::cout << "c1+c2: " << c1 + c2 << ", c2+c1: " << c2 + c1 << std::endl;
  std::cout << "c1+3: " << c1 + 3 << ", 3+c1: " << 3 + c1 << std::endl;
  return 0;
}
