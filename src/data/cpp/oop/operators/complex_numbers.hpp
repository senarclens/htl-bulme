// ... header guard etc
#include <iostream>

class Complex {
public:
  explicit Complex(double real, double imag) : real_{real}, imag_{imag} {}
  double real() const { return real_; }
  double imag() const { return imag_; }
  Complex operator+(const Complex& c) const;
  Complex operator+(double d) const { return Complex{real_ + d, imag_}; }
private:
  double real_ = 0.0;
  double imag_ = 0.0;
};
Complex operator+(double d, const Complex& c);  // cannot be declared in class
std::ostream& operator<<(std::ostream& out, const Complex& c);
// ...
