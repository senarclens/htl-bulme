// SPDX-FileCopyrightText: 2023 Gerald Senarclens de Grancy <oss@senarclens.eu>
// SPDX-License-Identifier: MIT

#include "account.hpp"

namespace bank {

unsigned int ChildAccount::withdraw(unsigned int amount) {
  if (amount > balance()) {
    amount = balance();
  }
  return Account::withdraw(amount);
}

}  // namespace bank
