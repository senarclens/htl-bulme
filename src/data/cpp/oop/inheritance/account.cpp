#include "account.hpp"

#include <iostream>
#include <string>

namespace bank {

unsigned long Account::next_number_ { 7483919474 };

Account::Account(std::string owner, unsigned int deposit)
    : balance_{deposit}, owner_{owner}  {
  number_ = next_number_;
  next_number_++;
}

void Account::deposit(unsigned int amount) {
  balance_ += amount;
}

/* return the amount actually withdrawn */
unsigned int Account::withdraw(unsigned int amount) {
  balance_ -= amount;
  return amount;
}


std::ostream& operator<<(std::ostream& out, const Account& a) {
  out << "Account(" << a.number() << ", " << a.balance() << ")";
  return out;
}

} // namespace bank
