#include <iostream>

#include "point.h"

namespace draw {

class Circle {
public:
  Circle() = default;
  Circle(double x, double y, double radius) : radius_(radius), center_(x, y) {};
  Circle(Point center, double radius) : radius_(radius), center_(center) {};
  void draw() { std::cout << "drawing Circle(" << center_.x() << "/"
    << center_.y() << ", " << radius_ << ")" << std::endl; }
private:
  double radius_ = 1.0;
  // as an alternative a Point* could be used (via new and delete in ctor);
  // this would require a dtor and should only be done for large objects
  Point center_;  // default value is default constructor of Point
};

}  // namespace draw

int main(void) {
  draw::Circle c1(3, 5, 2);
  c1.draw();
  draw::Point center{-2, -5};
  draw::Circle c2(center, 3);
  c2.draw();
  draw::Circle c3;
  c3.draw();
}
