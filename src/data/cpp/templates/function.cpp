#include <iostream>
#include <string>
#include <iomanip>
template <typename T>
T max(T x, T y) {
  return (x > y) ? x : y;
}

int main() {
  std::cout << std::fixed << std::setprecision(1);
  std::cout << max(3, 1) << std::endl;  // max<int>(int, int)
  std::cout << max(7.2, 8.4) << std::endl;  // max<double>(double, double)
  std::cout << max<double>(9, 4) << std::endl;  // explicit passing of type
  return 0;
}
