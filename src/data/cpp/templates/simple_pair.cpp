#include <iostream>
template <typename T>  // typename and class are equivalent here
struct Pair {
  T first;
  T second;
};

int main() {
  Pair<int> p1{5, 6};
  Pair<char> p2{'a', 'b'};
  std::cout << p1.first << ' ' << p1.second << std::endl;
  std::cout << p2.first << ' ' << p2.second << std::endl;
  return 0;
}
