#include <iostream>
template <class A, class B>  // typename and class are equivalent here
struct Pair {
  A first;
  B second;
};

int main() {
  Pair<int, char> p1{5, 'c'};
  Pair<char[4], double> p2{"Pat", 0.0315};
  std::cout << p1.first << ' ' << p1.second << std::endl;
  std::cout << p2.first << ' ' << p2.second << std::endl;
  return 0;
}
