#include <cstdint>
#include <limits>
#include "catch_amalgamated.hpp"
uint64_t add_to(uint32_t n);  // usually in header file
TEST_CASE("sum_to must calculate correctly", "[sum_to]") {
  // setup code that runs before each section is written here
  SECTION("regular use cases") {
    CHECK(add_to(1) == 1);  // if CHECK fails, TEST will fail, but continue
    CHECK(add_to(2) == 3);
    REQUIRE(add_to(25) == 325);  // if REQUIRE fails, TEST fails immediately
    CHECK(add_to(26) == 351);
    CHECK(add_to(500000000) == 125000000250000000);
  }
  SECTION("corner cases") {
    REQUIRE(add_to(0) == 0);
  }
  SECTION("slow cases") {
    CHECK(add_to(4294967293) == 9223372026117357571);
    CHECK(add_to(4294967294) == 9223372030412324865);

  }
}
TEST_CASE("sum_to must terminate for all valid input data",
          "[sum_to_edge]") {
  // does not terminate for the old code version due to an overflow of the
  // loop counter (the loop would terminate only at UINT_MAX + 1)
  REQUIRE(add_to(std::numeric_limits<uint32_t>::max())
          == 9223372034707292160);
}
