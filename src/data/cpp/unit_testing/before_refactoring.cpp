#include <cstdint>
// Return the sum of all values from 1 to n.
uint64_t add_to(uint32_t n) {
    uint64_t sum = 0;
    for (uint32_t i = 1; i <= n; ++i) {
        sum += i;
    }
    return sum;
}
