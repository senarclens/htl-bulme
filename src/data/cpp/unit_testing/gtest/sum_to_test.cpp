#include <cstdint>
#include <limits>
#include "gtest/gtest.h"

uint64_t add_to(uint32_t n);  // usually in header file

TEST(sum_to, fast_test) {
  EXPECT_EQ(add_to(0), 0);
  EXPECT_EQ(add_to(1), 1);
  EXPECT_EQ(add_to(2), 3);
  EXPECT_EQ(add_to(25), 325);  // EXPECT leads to failure but continues
  ASSERT_EQ(add_to(26), 351);  // ASSERT terminates test immediately
  EXPECT_EQ(add_to(500000000), 125000000250000000);
}

TEST(sum_to, slow_test) {
  EXPECT_EQ(add_to(4294967293), 9223372026117357571);
  EXPECT_EQ(add_to(4294967294), 9223372030412324865);
}

// does not terminate for the old code version due to an overflow of the
// loop counter (the loop would terminate only at UINT_MAX + 1)
TEST(sum_to, edge_test) {
  EXPECT_EQ(add_to(std::numeric_limits<uint32_t>::max()),
            9223372034707292160);
}
