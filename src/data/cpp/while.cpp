#include <iostream>

int main() {
  int n = 10;
  while (n > 0) {  // execute block while expression evaluates to `true`
    std::cout << n << ", ";
    --n;  // avoid side effects in statement above
  }
  std::cout << "FIRE!" << std::endl;
  return 0;
}
