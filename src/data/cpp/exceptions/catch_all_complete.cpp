#include <iostream>

class C {
public:
  C() { std::cout << "creating a temporary file (just pretend)" << std::endl; }
  ~C() { std::cout << "removing a temporary file (just pretend)" << std::endl; }
};

void do_something() {
  // code, that deep in the call stack might throw an unexpected exception
  throw 1;
}

int main() {
  C c;
  try {
    do_something();
  }
  catch( ... ) {
    std::cerr << "Abnormal termination\n";  // shouldn't happen
  }
  // now we can be certain that stack unwinding takes place
  return 0;
}
