#include <iostream>
#include <sstream>
#include <string>

class InvalidCircleException : std::exception {
public:
  InvalidCircleException(double radius) : message_{create_message(radius)} {};
  const char* what() const noexcept override {
    return message_.c_str();
  }
private:
  std::string create_message(double radius) {
    std::stringstream message;
    message << "A circle must not have a negative radius (";
    message << radius << ")!";
    return message.str();
  }
  std::string message_;
};

class Circle {
public:
  Circle(double x, double y, double _radius) : x_{x}, y_{y} { radius(_radius); }
  double x() const { return x_; }
  double y() const { return y_; }
  double radius() const { return radius_; }
  void radius(double radius) {
    if (radius < 0) {
        throw InvalidCircleException(radius);
    }
    radius_ = radius;
  }
private:
  double x_{0.0};
  double y_{0.0};
  double radius_{0.0};
};

std::ostream& operator<<(std::ostream& out, const Circle& c) {
  out << "Circle(x=" << c.x() << ", y=" << c.y() << ", radius=" << c.radius();
  out << ")";
  return out;
}

int main() {
  try {
    Circle a{25, 15, 7};  // ok
    std::cout << a << std::endl;
    Circle invalid{25, 15, -7};  // throws
    std::cout << invalid << std::endl;
  } catch ( const InvalidCircleException& e ) {
    std::cout << e.what() << std::endl;
  } catch ( ... ) {
    std::cout << "something terrible happened" << std::endl;
  }
  return 0;
}
