#include <iostream>
void do_something() {
  // code, that deep in the call stack might throw an unexpected exception
  throw 1;
}

int main() {
  // imagine a local variable that needs to be cleaned up
  try {
    do_something();
  }
  catch( ... ) {
    std::cerr << "Abnormal termination\n";  // shouldn't happen
  }
  // now we can be certain that stack unwinding takes place
  return 0;
}
