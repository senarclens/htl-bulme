#include <fstream>
#include <iostream>
#include <stdexcept>

void read_file(const std::string& filename) {
  std::ifstream file(filename);
  if (!file.is_open()) {
    throw std::runtime_error("Failed to open file: " + filename);
  }
  // ...
}

int main() {
  try {
    read_file("file_io_error.cpp"); // file might exist
    read_file("missing_file"); // exception is thrown
  } catch ( const std::runtime_error& e ) {
    std::cout << e.what() << std::endl;
  } catch ( ... ) {
    std::cout << "something terrible happened" << std::endl;
  }
  return 0;
}
