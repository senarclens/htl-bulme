#include <fstream>  // provides `fstream`, `ifstream`, `ofstream`
#include <iostream>  // for `cerr`
int main() {
  int num = 15;
  std::ofstream fs("data.txt");  // open for writing

  if (!fs) {  // check if opening was successful
    std::cerr << "Error!" << std::endl;
    return 1;
  }

  fs << num << std::endl;
  fs << "This is a text" << std::endl;

  return 0;
}
