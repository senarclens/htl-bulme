#include <fstream>  // provides `fstream`, `ifstream`, `ofstream`
#include <iostream>  // for `cerr`
int main() {
  std::ifstream fs("data.txt");  // open for reading
  int num(0);

  if (fs) {  // check if opening was successful
    std::string s;

    fs >> num;  // read `int` until first whitespace character
    while (std::getline(fs, s)) {  // read line by line
      std::cout << "READ: " << s << std::endl;
    }
  }
  std::cout << "2 * num: " << 2 * num << std::endl;
  return 0;
}

