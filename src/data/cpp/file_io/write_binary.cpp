#include <fstream>
struct Data {
  int first;
  int second;
};
int main() {
  Data d = {.first=1724, .second=255};
  std::ofstream fs("data.bin", std::ios::binary);
  if (fs) {
    fs.write(reinterpret_cast<char*>(&d), sizeof(Data));
  }
  return 0;
}
