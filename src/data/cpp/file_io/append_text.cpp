#include <fstream>  // provides fstream, ifstream, ofstream
#include <iomanip>
#include <iostream>  // for `cerr`
int main() {
  std::ofstream fs("data.txt", std::ios::app);  // open for appending

  if (!fs) {  // check if opening was successful
    std::cerr << "Error!" << std::endl;
    return 1;
  }

  fs << std::fixed << std::setprecision(2) << 36.8 << std::endl;
  fs << "Additional text" << std::endl;
  return 0;
}
