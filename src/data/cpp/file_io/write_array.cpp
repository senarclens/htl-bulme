#include <fstream>
#include <limits>
#include <vector>
using std::numeric_limits, std::ofstream, std::vector;
int main() {
  vector<unsigned> v = {0, numeric_limits<unsigned>::max(), 255, 0xff000000, 1724};
  std::ofstream fs("array.bin", std::ios::binary);
  if (fs) {
    fs.write(reinterpret_cast<const char *>(v.data()), v.size() * sizeof(unsigned));
  }
  return 0;
}
