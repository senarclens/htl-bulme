#include <fstream>
#include <iostream>
struct Data {
  int first;
  int second;
};
int main() {
  Data d;
  std::ifstream fs("data.bin", std::ios::binary);
  if (fs) {
    fs.read(reinterpret_cast<char*>(&d), sizeof(d));
  }
  std::cerr << "first: " << d.first << " second: " << d.second << std::endl;
  return 0;
}
