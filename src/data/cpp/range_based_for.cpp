#include <iostream>
#include <string>
#include <vector>

using std::cout;

int main() {
  std::vector<std::string> names = {"Pat", "Chris", "Sue", "Steve", "Anne"};
  for (const auto& name : names) {
    cout << name << " ";
  }
  cout << "\n";
}
