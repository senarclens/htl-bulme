#include <cstdint>
#include <iostream>
#include <limits>

// Return the sum of all values from 1 to n.
uint64_t add_to_on(uint32_t n) {
    uint64_t sum = 0;
    for (uint64_t i = 1; i <= n; ++i) {
        sum += i;
    }
    return sum;
}

uint64_t add_to_o1(uint32_t n) {
  // doubles cannot represent numbers close to <uint64>::max() correctly
  // division by `2.0` leads to wrong result if n is close to <uint32>::max()
  if (n % 2) return ((n + 1ull) / 2) * n;
  return (n / 2 * (n + 1ull));
}

void print_usage(char* command) {
    std::cout << "Usage: " << command << " {1|n}" << std::endl;
}

int main(int argc, char** argv) {
  // ::max()-1 (0xfffffffe) as 4294967295 does not terminate for O(n) impl.
  uint32_t n = std::numeric_limits<uint32_t>::max() - 1;
  if (argc != 2) { print_usage(argv[0]); return 1; }
  switch (*argv[1]) {
  case '1':
    std::cout << "result: " << add_to_o1(n) << std::endl; break;
  case 'n':
    std::cout << "result: " << add_to_on(n) << std::endl; break;
  default:
    print_usage(argv[0]); return 1;
  }
  return 0;
}
