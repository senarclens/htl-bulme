#include <algorithm>  // std::sort
#include <iostream>
#include <vector>


int main () {
  std::vector<int> values{ 33, 12, 99, 90, 7, 45 };
  std::sort(values.begin(), values.end());
  for (const auto& value : values) {
     std::cout << value << " ";
  }
  std::cout << std::endl;
  return 0;
}
