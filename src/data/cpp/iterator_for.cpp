#include <iostream>
#include <string>
#include <vector>

int main() {
  std::vector<std::string> names = {"Pat", "Chris", "Sue", "Steve", "Anne"};
  // use reverse iterators
  for (auto it = names.crbegin(); it != names.crend(); it++) {
    std::cout << *it << std::endl;  // dereference the iterator
  }
  std::cout << std::endl;
  return 0;
}
