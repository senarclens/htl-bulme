#include <iostream>

int main(void) {
  size_t size = 8192 * 1024;  // 8 M * 4 B
  int* array = new int[size];  // 32 MB

  for (size_t i = 0; i < size / sizeof(int); ++i) {
    array[i] = i * i;  // initialize the array ...
  }
  std::cout << "Survived?\n";
  delete[] array;  // avoid memory leak
  return 0;
}
