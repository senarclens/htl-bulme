#include <iostream>

int main(void) {
  const size_t size = 12;  // any value determined at runtime
  int* array = new int[size];  // array on the free store
  int array_on_stack[3];  // array on stack (see visualization!)
  for (size_t i = 0; i < size; ++i) {
    array[i] = i * i;  // initialize the array ...
  }
  for (size_t i = 0; i < 3; ++i) {  // use the array ...
    std::cout << "array[" << i << "]: " << array[i] << std::endl;
  }
  delete[] array;  // otherwise memory will grow when the program keeps running
  return 0;
}
