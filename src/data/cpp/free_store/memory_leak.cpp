
struct Point { double x; double y; };
void leak_memory() {
  Point* points = new Point[2];
  points[0] = (Point) { .x=4, .y=7 };
  points[1] = (Point) { .x=-5, .y=3 };
}

int main(void) {
  for (int i(0); i < 5; ++i) {
    leak_memory();
  }
  return 0;
}
