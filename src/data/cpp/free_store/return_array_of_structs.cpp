#include <iomanip>
#include <iostream>

struct Point { double x; double y; };
Point* fetch_points() {
  Point* points = new Point[2];
  points[0] = (Point) { .x=4, .y=7 };
  points[1] = (Point) { .x=-5, .y=3 };
  return points;
}

int main(void) {
  Point* points = fetch_points();
  std::cout << std::fixed << std::setprecision(2);
  std::cout << "Point 1: " << std::setw(5) << points[0].x;
  std::cout << " / " << points[0].y << std::endl;
  std::cout << "Point 2: " << points[1].x << " / " << points[1].y << std::endl;
  delete[] points;
  return 0;
}
