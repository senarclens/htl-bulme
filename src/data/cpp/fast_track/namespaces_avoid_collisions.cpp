#include <iostream>
namespace a {
  void say_hello() {
    std::cout << "Hello!" << std::endl;
  }
}  // namespace a
namespace b {
  void say_hello() {
    std::cout << "Bonjour!" << std::endl;
  }
}  // namespace b

int main(void) {
  a::say_hello();
  b::say_hello();
  return 0;
}


