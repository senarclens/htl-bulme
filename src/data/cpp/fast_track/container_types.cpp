#include <iostream>
#include <string>
#include <vector>

int main() {
  char sep('\t');  // single character (escaped tab key)
  std::string name("Amelie");  // C++ string
  std::vector<double> numbers(3);  // a managed array of doubles
  std::cout << name << std::endl;
  for (int i = 0; i < numbers.size(); ++i) {
    std::cout << numbers[i] << sep;
  }
  std::cout << std::endl;
  return 0;
}
