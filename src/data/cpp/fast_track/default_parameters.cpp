#include <iostream>

void increment(int& i, int value = 1);

int main(void) {
  int num {5};
  increment(num, 5);
  increment(num);
  increment(num);
  std::cout << "num: " << num << std::endl;
  return 0;
}

void increment(int& i, int value) {
  i += value;
}
