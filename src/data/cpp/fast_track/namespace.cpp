#include <iostream>

namespace ns {
  void say_hello();
}  // namespace ns

int main(void) {
  ns::say_hello();
  return 0;
}

namespace ns {
  void say_hello() {
    std::cout << "Hello!" << std::endl;
  }
}  // namespace ns

