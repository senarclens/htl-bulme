#include <iostream>
#include <vector>
void print_vector(std::vector<int>& v) {
  for (int elem : v) std::cout << elem << " ";
  std::cout << std::endl;
}
int main() {
  std::vector<int> v1(5);  // create vector {0, 0, 0, 0, 0}
  std::vector<int> v2 = v1;  // this is copy assignment, not a reference
  std::vector<int>& v3 = v1;  // this is a reference
  v1[1] = 7;
  v2[3] = 2;
  v3[2] = 4;
  print_vector(v1);
  print_vector(v2);
  print_vector(v3);
}
