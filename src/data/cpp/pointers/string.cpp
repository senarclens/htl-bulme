#include <iostream>
#include <cctype>

void upper(char* text) {
  while (*text) {  // string ends with the `\0` character
    *text = toupper(*text);  // return upper case character
    text++;
  }
}

int main() {
  char name[] = "Chris";
  upper(name);
  std::cout << name << std::endl;
  return 0;
}
