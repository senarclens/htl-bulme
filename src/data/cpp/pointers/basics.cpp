#include <iostream>

int main() {
    int a = 5;
    int* a_ptr = &a;  // &: address-of operator
    *a_ptr += 5;  // *: dereference operator
    std::cout << "updated value: " << a << " (at " << a_ptr << " - ";
    std::cout << sizeof(a_ptr) << " bytes)" << std::endl;
    return 0;
}
