#include <cstdio>
struct IsoDate {int year; int month; int day;};

void print_date(const IsoDate* d) {
  printf("%04d-%02d-%02d\n", (*d).year, d->month, d->day);
}

int main() {
  IsoDate date = {2022, 1, 11};
  print_date(&date);
  return 0;
}
