#include <iostream>

int sum(const int* array, int count) {
  int result = 0;
  while (count--) {
    result += *array++;
  }
  return result;
}

int main() {
  int array[] = {1, 2, 3, 4, 5, 6};
  int value = sum(array, sizeof(array) / sizeof(int));
  std::cout << "sum: " << value << std::endl;
  return 0;
}
