#include <iostream>

int main() {
  int a = 4, b = 5;
  int* a_ptr = &a;  // when starting, it is easiest to have a pre- or suffix
  int* b_ptr = &b;  // for every pointer in your programs
  int** ptr_ptr = &a_ptr;
  *ptr_ptr = nullptr;  // set a_ptr to invalid location
  *ptr_ptr = b_ptr;
  **ptr_ptr *= **ptr_ptr;  // use multiple lines to make readable
  std::cout << "a = " << a << ", b = " << b << std::endl;
  return 0;
}
