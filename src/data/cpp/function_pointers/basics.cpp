#include <iostream>

int sum(int a, int b) { return a + b; }

int main(void) {
  int (*sum_ptr)(int,int);  // declares a function pointer named `sum_ptr`
  sum_ptr = sum;  // same as `sum_ptr = &sum;`
  std::cout << "The sum of 2 + 3 is " << (*sum_ptr)(2, 3) << ".\n";
  std::cout << "The sum of 2 + 3 is " << (sum_ptr)(2, 3) << ".\n";
  std::cout << "The sum of 2 + 3 is " << sum_ptr(2, 3) << ".\n";
  return 0;
}
