#include <algorithm>
#include <iostream>
#include <stdlib.h>

using std::cout;

namespace ia {
struct IntArray;
struct IntArray {
  size_t reserved;  // available storage
  size_t elements;  // current number of elements
  int* array;

  void (*push_back)(IntArray* self, int elem);
  long int (*sum)(const IntArray* self);
  void (*print)(const IntArray* self);
  IntArray* (*destruct)(IntArray* self);
};

IntArray* destruct(IntArray* self) {
  delete[] self->array;
  self->array = (int*) NULL;
  delete self;
  return (IntArray*) NULL;
}

void push_back(IntArray* self, int elem) {
  if (self->elements == self->reserved) {
    int* old_data = self->array;
    self->reserved *= 2;
    self->array = new int[self->reserved];
    std::copy_n(old_data, self->elements, self->array);
    delete[] old_data;
  }
  self->array[self->elements] = elem;
  self->elements++;
}

long int sum(const IntArray* self) {
  long int sum = 0;
  for (size_t i = 0; i < self->elements; ++i) {
    sum += self->array[i];
  }
  return sum;
}

void print(const IntArray* self) {
  if (!self->elements) {
    cout << "[]\n" ;
  } else {
    cout << "[";
    for (size_t i = 0; i < self->elements - 1; ++i) {
      cout << self->array[i] << ", ";
    }
    cout << self->array[self->elements - 1] << "]\n";
  }
}

IntArray* construct() {
  IntArray* self = new IntArray;
  self->reserved = 100;
  self->elements = 0;
  self->array = new int[self->reserved];
  self->print = print;
  self->sum = sum;
  self->push_back = push_back;
  self->destruct = destruct;
  return self;
}
}  // namespace ia

int main(void) {
  ia::IntArray* a = ia::construct();
  a->push_back(a, 10);
  a->push_back(a, 3);
  a->push_back(a, 7);
  a->push_back(a, 12);
  a->push_back(a, 8);
  a->print(a);
  cout << "The sum of all elements is " << a->sum(a) << ".\n";
  a = a->destruct(a);
  return 0;
}
