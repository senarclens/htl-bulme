#include <iostream>
#include <curl/curl.h>

int main(void) {
  CURL* curl;
  CURLcode res;
  curl = curl_easy_init();
  if (curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://study.find-santa.eu/");
    res = curl_easy_perform(curl);
    if (res != CURLE_OK)  // check of errors
      std::cerr << "curl_easy_perform() failed: "
                << curl_easy_strerror(res) << std::endl;
    curl_easy_cleanup(curl);
  }
  return 0;
}
