#include <cstdlib>
#include <cstring>
#include <iostream>

int case_insensitive_compare(const void* a, const void* b) {
  return strcasecmp(*(const char**)a, *(const char**)b);
}

int main(void) {
  const char* names[] = {"Harry", "Anne", "Mary", "chris", "Pat", "eric", "Lily"};
  qsort(names, 7, sizeof(const char*), case_insensitive_compare);
  std::cout << "names (sorted ascending)" << std::endl;
  for (int i = 0; i < 7; ++i) {
    std::cout << names[i] << std::endl;
  }
  return 0;
}
