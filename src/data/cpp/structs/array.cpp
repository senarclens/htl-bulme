#include <iostream>
typedef struct {
  int id;
  char name[25];
  unsigned int grades[8];
} Student;

Student read_student(int id) {
  // get data from eg. a database
  Student s = { id, "Pat Kaling", { 1, 1, 2, 1, 3, 2, 1, 1 } };
  return s;
}
int main() {
  Student s = read_student(123);
  std::cout << s.name << " has a " << s.grades[3] << " in English.";
  return 0;
}
