#include <iomanip>
#include <iostream>
using std::cout, std::fixed, std::setprecision;
struct Point {
  float x;
  float y;
};
void print_point(Point p) {
  cout << fixed << setprecision(2) << "P(" << p.x << "/" << p.y << ")\n";
  p.x = 0;  // does not affect p in main
}

int main() {
  Point p = { .x=1.5 , .y=3.0 };
  print_point(p);  // pass by value
  return 0;
}
