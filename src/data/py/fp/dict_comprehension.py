#!/usr/bin/env python3

"""
Simple dict comprehension exchanging the keys and values of a dict.

Assumes that the values are unique and hashable.
"""

d = {'Martin': 7, 'Sue': 5, 'Pat': 12, 'Chris': 4}
print(d)
d = {v: k for k, v in d.items()}
print(d)
