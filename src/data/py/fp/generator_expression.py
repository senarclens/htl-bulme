#!/usr/bin/env python3

squares = (x ** 2 for x in [1, 2, 3, 4, 5])
print(squares)
print(list(squares))
