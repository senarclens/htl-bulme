numbers = [1, 3, 17]
if all(map(lambda x: x % 2, numbers)):
    print('only odd numbers')
numbers.append(2)
if not all(map(lambda x: x % 2, numbers)):
    print('not just odd numbers')
