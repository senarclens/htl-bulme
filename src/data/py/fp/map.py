#!/usr/bin/env python3

squares = map(lambda x: x ** 2, [1, 2, 3, 4, 5])
print(squares)  # nothing was computed yet
print(list(squares))
