#!/usr/bin/env python3

import functools
product = functools.reduce(lambda x, y: x * y, [1, 2, 3, 4, 5])
print(product)
