#!/usr/bin/env python3

names = ['betty', 'Anna', 'Sue', 'gernot', 'Luis', 'Therese', 'amelie', ]
names.sort()  # likely not what we'd expect
names.sort(key=lambda x: x.lower())
