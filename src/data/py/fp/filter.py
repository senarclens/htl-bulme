#!/usr/bin/env python3

odd_numbers = filter(lambda x: x % 2, [1, 2, 3, 4, 5])
print(odd_numbers)  # nothing was computed yet
print(list(odd_numbers))
