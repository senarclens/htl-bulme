from os import linesep
# ... long computation
result = [[1, 3, 2],
          [4, 5, 6],
          [9, 8, 7]]
with open('result.txt', 'a', encoding='utf-8') as outfile:  # a => append
    outfile.write("Now we really solve the world's problems!" + linesep)
    for row in result:
        for elem in row:
            print(str(elem), file=outfile, end=' ')
        outfile.write(linesep)
