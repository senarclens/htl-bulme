def fib(n):
    """
    Return the n-th fibonacci number (starting at 1).

    Note: this is an inefficient implementation.
    """
    if n <= 1:  # base case
        return n
    return fib(n - 1) + fib(n - 2)  # recursive case

print(fib(5))
