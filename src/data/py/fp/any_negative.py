from collections import namedtuple

class Result:
    def __init__(self, name, grade):
        self.name = name
        self.grade = grade

results = [Result('Joe', 3), Result('Maggie', 2), Result('Josh', 1),
           Result('Sue', 2)]
if not any(map(lambda r: r.grade == 5, results)):
    print('All students passed.')
results.append(Result('Francis', 5))
if any(map(lambda r: r.grade == 5, results)):
    print('At least one student failed.')
