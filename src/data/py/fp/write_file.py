from os import linesep
# ... long computation
result = [[1, 2, 3],
          [4, 5, 6],
          [7, 8, 9]]
with open('result.txt', 'w', encoding='utf-8') as outfile:  # w => write
    outfile.write("These numbers solve the world's problems!" + linesep)
    for row in result:
        for elem in row:
            outfile.write(str(elem) + ' ')
        print(file=outfile)
