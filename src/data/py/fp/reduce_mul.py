#!/usr/bin/env python3

import functools
from operator import mul
product = functools.reduce(mul, [1, 2, 3, 4, 5])
print(product)
