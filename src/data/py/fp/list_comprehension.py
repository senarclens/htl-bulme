#!/usr/bin/env python3

squares = [x ** 2 for x in [1, 2, 3, 4, 5]]  # ~map
print(squares)
odd_numbers = [x for x in [1, 2, 3, 4, 5] if x % 2]  # ~filter
print(odd_numbers)
