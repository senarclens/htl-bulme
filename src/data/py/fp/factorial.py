#!/usr/bin/env python3

def factorial(n):
    """
    Return the factorial of n.
    """
    assert n >= 0
    if n <= 1:  # base case
        return 1
    return n * factorial(n - 1)  # recursion: use solution to smaller problem

print(factorial(5))
