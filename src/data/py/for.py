#!/usr/bin/env python3

A_STRING = 'What a beautiful snake!'
for char in A_STRING:
    print(char)
for index, char in enumerate(A_STRING):
    print(f'{index:2}: {char}')
