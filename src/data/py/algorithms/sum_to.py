import argparse
import sys


def add_to_on(n):
    """
    Return the sum of all values from 1 to n.
    """
    sum = 0
    for i in range(n+1):
        sum += i
    return sum


def add_to_o1(n):
    """
    Return the sum of all values from 1 to n.
    """
    return n * (n + 1) // 2


def main():
    parser = argparse.ArgumentParser(
        description='Run function with different asymptotic complexities.')
    parser.add_argument('complexity', choices=['constant', 'linear'],
        help='Asymptotic complexity -- constant (O1) or linear(On)')
    args = parser.parse_args()
    limit = 0xffffffff
    print(f'Running with n={limit} (UINT32_MAX) '
          f'and {args.complexity} complexity.')
    match args.complexity:
        case 'constant':
            print('result:', add_to_o1(sys.maxsize))
        case 'linear':
            print('result: ', add_to_on(sys.maxsize))
    return 0


if __name__ == '__main__':
    sys.exit(main())
