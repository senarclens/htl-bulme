#!/usr/bin/env python

def power(base, exp=2):
    return base ** exp

power(3, 3)  # -> 27
power(5)  # -> 25
