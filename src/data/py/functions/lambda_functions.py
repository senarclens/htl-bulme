#!/usr/bin/env python3

a_list = [3, 4, 19, 11, 1, 4, 6, 8, 5, 2, 0, 12, 13, 3]
a_list.sort(key=lambda x: x % 3)  # sort by remainder of division by 3
print(a_list)
