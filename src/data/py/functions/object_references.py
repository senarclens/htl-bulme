#!/usr/bin/env python3

def print_uppercase(text):
    """
    Print the passed text as upper case.
    """
    print(text.upper())
a_text = "Programming is fun!"
result = print_uppercase(a_text)

def print_sorted_list(l):
    l.sort()
    print(l)
a_list = [5, 3, 7, 1, 4]
another_result = print_sorted_list(a_list)
