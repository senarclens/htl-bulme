#!/usr/bin/env python3

def interest(capital, rate, years=1):
    """
    Return the total interest of investing `capital` at `rate`.
    """
    value = capital * (1 + rate) ** years
    return value - capital

print(interest(100, 0.03))
result = interest(100, 0.03, 3)
profit = interest(1300, 0.025, 5)
