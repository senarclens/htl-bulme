#!/usr/bin/env python3

def interest(capital=1000, rate=0.04, years=1):
    """
    Return the total interest of investing `capital` at `rate`.
    """
    value = capital * (1 + rate) ** years
    return value - capital

print(interest(100, 0.03))
profit = interest(capital=1300, rate=0.025, years=5)
profit = interest(rate=0.03, years=3, capital=1200)
profit = interest(years=5)
profit = interest(1300, years=5)
result = interest(1000 + interest(rate=0.06), 0.03, 3)
