"""
Simple implementation of rot13.
"""

def encode(text: str) -> str:
    """Return `text` "encrypted" using the rot13 algorithm."""
    result = []
    for char in text:
        if ('A' <= char <= 'M') or ('a' <= char <= 'm'):
            num = ord(char) + 13
        elif ('N' <= char <= 'Z') or ('a' <= char <= 'z'):
            num = ord(char) - 13
        else:
            num = ord(char)
        result.append(chr(num))
    return "".join(result)


def decode(text: str) -> str:
    """Return `text` "decoded" using the rot13 algorithm."""
    return encode(text)

if __name__ == '__main__':
    print(encode('apt-get moo'))
    print(decode(encode('apt-get moo')))
