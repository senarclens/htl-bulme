#!/usr/bin/env python3

a_string = 'What a beautiful snake!'
a_string.upper()
print(a_string)
a_string = a_string.upper()
print(a_string)

print()
print(a_string.split())
a_string = 'Steve, Paul, Susan, Anna, Peter, Amy'
print(a_string.split(', '))
