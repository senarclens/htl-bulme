from typing import TypeAlias
Iterable: TypeAlias = list | tuple  # variable annotation as `TypeAlias`

def demo(first: list, second: Iterable) -> int:
    """
    Demonstrate type hints.
    """
    first.extend(second)
    count: int = len(first)  # variable annotation as `int`
    return count
