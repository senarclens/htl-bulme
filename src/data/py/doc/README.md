Don't name any module `doctest.py` as doing so would run that module instead
of the standard library module when issuing `python -m doctest`.
