def add(first: float, second: float) -> float:
    """
    Return the sum of the parameters.
    """
    return first + second
