"""
.. moduleauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>

This module provides basic mathematical functions.
"""

def add(first, second):
    """
    Return the sum of the parameters.
    """
    return first + second

print(__doc__)  # access the module documentation
print(add.__doc__)  # access the documentation for `add`
if __name__ == '__main__':
    help(add)  # access interactive help in the interpreter; `add?` in ipython
