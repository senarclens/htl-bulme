def add(first: float, second: float) -> float:
    """
    Return the sum of the parameters.

    >>> add(1,3)
    4
    >>> add(7.3, -3.0)
    4.3
    >>> add(42, "foo")
    Traceback (most recent call last):
        ...
    TypeError: unsupported operand type(s) for +: 'int' and 'str'
    """
    return first + second
