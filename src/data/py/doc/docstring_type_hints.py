def add(first, second):
    """
    Return the sum of the parameters as integer or floating point number.

    Arguments:
    first -- an integer or floating point number
    second -- an integer or floating point number
    """
    return first + second
