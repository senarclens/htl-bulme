PRECISION = 2  # 2 digits are a legal requirement (see ...)
DEFAULT_POSTAL_CODE = 8010  # most of our customers have this postal code
POSTAL_CODE_KEY = "postal_code"
# ...
rounded_profit = round(profit, PRECISION)
rounded_effort = round(effort, PRECISION)
postal_code = data.get(POSTAL_CODE_KEY, DEFAULT_POSTAL_CODE)
