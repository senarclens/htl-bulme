"""Demonstrate how type hints can help to avoid issues."""

import sys
from typing import TypeAlias
Iterable: TypeAlias = list | tuple


def demo(first: list, second: Iterable) -> list:
    """Demonstrate the purpose of type hints."""
    result: list = first.extend(second)  # variable annotation
    return result


def main() -> int:
    """Print the result of `demo(.)` to stdout."""
    print(demo([1, 2, 3], (4, 5, 6)))
    return 0


if __name__ == "__main__":
    sys.exit(main())
