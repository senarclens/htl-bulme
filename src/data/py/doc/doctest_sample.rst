The ``doctest_sample`` module
=============================
Using ``add``
-------------

This is an example text file in reStructuredText format.  First import
``add`` from the ``doctest_sample`` module:

    >>> from doctest_sample import add

Now use it:

    >>> add(6, 5)
    120

Documentation like this can be used by the Sphinx documentation generator.
