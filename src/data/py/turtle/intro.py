#!/usr/bin/env python

import turtle as t
t.setup(width=600, height=400)

t.forward(100)
t.left(90)
t.forward(100)
t.left(90)

# exercise: finish a complete square

t.done()
