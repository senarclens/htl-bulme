import cmath
import math

def quadratic(a, b, c):
    """
    Compute the solution to a quadratic equation.
    """
    root = b ** 2 - 4 * a * c
    if root >= 0:
        sqrt = math.sqrt
    else:
        sqrt = cmath.sqrt
    x1 = (-b + sqrt(root)) / (2 * a)
    x2 = (-b - sqrt(root)) / (2 * a)
    return x1, x2
