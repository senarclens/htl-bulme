#!/usr/bin/env python3
import tkinter as tk  # requires having package `tk` installed
from solver import quadratic
def click_solve() -> None:
    """Compute and print the roots of a quadratic equation."""
    x1, x2 = quadratic(a.get(), b.get(), c.get())
    print('x1:', x1, 'x2:', x2)

app = tk.Tk()
a, b, c = tk.DoubleVar(), tk.DoubleVar(), tk.DoubleVar()
app.title('Solver for Quadratic Equations')
inputs = {' x^2': a, ' x + ': b, ' = 0': c}
for text, var in inputs.items():
    tk.Entry(app, width=3, textvariable=var).pack(side=tk.LEFT, pady=10)
    tk.Label(text=text).pack(side=tk.LEFT, pady=10)
tk.Button(app, text='solve', command=click_solve).pack(side=tk.LEFT, padx=10)
app.mainloop()
