#!/usr/bin/env python3
import sys

# lots of code here
def main():
    print("executed as program")
    return 0  # success

if __name__ == "__main__":  # False if this module is imported
    sys.exit(main())  # OS gets return value of main()
