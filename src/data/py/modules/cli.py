#!/usr/bin/env python3
"""
Command-line interface for solving quadratic equations.
"""
import sys
from solver import quadratic

def main():
    a = float(input("Enter the value for a: "))
    b = float(input("Enter the value for b: "))
    c = float(input("Enter the value for c: "))
    x1, x2 = quadratic(a, b, c)
    print('x1:', x1, 'x2:', x2)
    return 0

if __name__ == '__main__':
    sys.exit(main())
