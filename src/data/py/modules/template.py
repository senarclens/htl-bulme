#!/usr/bin/env python
"""
.. module:: example
    :synopsis: One sentence describing what your `example` module does
.. moduleauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>

Detailed description of what the module can do.
"""
# standard library imports first (sorted alphabetically)
import os
import sys
# third party library imports next
import numpy
# finally import own modules

def main():
    return 0

if __name__ == "__main__":
    sys.exit(main())

# meta information for documentation purposes
__copyright__ = '2024, Gerald Senarclens de Grancy'
__license__ = 'GPLv3'
