import math

def sin(x):
    return f'this function does not affect sin({x}) from the math module'

print(sin(math.pi))
print(math.sin(math.pi))
