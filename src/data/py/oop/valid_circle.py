#!/usr/bin/env python3
class Circle:
    def __init__(self, x, y, radius):
        self.x = x
        self.y = y
        self.radius = radius  # this is now safe

    def __str__(self):
        return (f'Circle(x={self.x}, y={self.y}, radius={self.radius})')

    @property
    def radius(self):
        return self.__radius

    @radius.setter
    def radius(self, radius):
        """Ensure that `radius` is valid."""
        assert radius >= 0, 'radius must be greater than or equal 0'
        self.__radius = radius


if __name__ == '__main__':
    a_circle = Circle(25, 15, 7)  # ok
    print(a_circle)
    another_circle = Circle(25, 15, -7)  # not allowed anymore
