#!/usr/bin/env python3
class Circle:
    def __init__(self, x, y, radius):  # special method to initialize object
        self.x = x  # self is a reference to the current instance
        self.y = y
        self.radius = radius

    def __str__(self):  # special method to represent object as string
        return (f'Circle(x={self.x}, y={self.y}, radius={self.radius})')


# an instance is created by "calling" the class with its required parameters
a_circle = Circle(25, 15, 7)  # ok
print(a_circle)
another_circle = Circle(25, 15, -7)  # still invalid
print(another_circle)
