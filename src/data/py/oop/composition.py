#!/usr/bin/env python3

import math


class Point:
    def __init__(self, x, y):
        self.__x = x
        self.__y = y

    def __str__(self):
        return (f'Point(x={self.x}, y={self.y})')

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y


class Circle:
    def __init__(self, center, radius):
        self.__center = center
        self.radius = radius

    def __str__(self):
        return (f'Circle(center={self.center}, radius={self.radius})')

    @property
    def center(self):
        return self.__center

    @property
    def radius(self):
        return self.__radius

    @radius.setter
    def radius(self, radius):
        """Ensure that `radius` is valid."""
        assert radius >= 0, 'radius must be greater than or equal 0'
        self.__radius = radius

    def area(self):
        """Return the area of the circle."""
        return math.pi * self.radius * self.radius

    def circumference(self):
        """Return the circumference of the circle."""
        return 2 * math.pi * self.radius


if __name__ == '__main__':
    a_circle = Circle(Point(25, 15), 7)
    print(a_circle.center)
    print(a_circle)
