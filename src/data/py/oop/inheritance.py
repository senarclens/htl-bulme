#!/usr/bin/env python3

import collections
import math


class Shape:
    """Every shape has an area and a circumference."""
    def area(self):
        raise NotImplementedError

    def circumference(self):
        raise NotImplementedError


class Point:
    def __init__(self, x, y):
        self.__x = x
        self.__y = y

    def __str__(self):
        return (f'Point(x={self.x}, y={self.y})')

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    def distance(self, other):
        """Return the euclidean distance between self and other."""
        delta_x = self.x - other.x
        delta_y = self.y - other.y
        return math.hypot(delta_x, delta_y)


class Circle(Shape):
    def __init__(self, center, radius):
        self.__center = center
        self.radius = radius

    def __str__(self):
        return ('Circle(center={self.center}, radius={self.radius})')

    @property
    def center(self):
        return self.__center

    @property
    def radius(self):
        return self.__radius

    @radius.setter
    def radius(self, radius):
        """Ensure that `radius` is valid."""
        assert radius >= 0, 'radius must be greater than or equal 0'
        self.__radius = radius

    def area(self):
        """Return the area of the circle."""
        return math.pi * self.radius * self.radius

    def circumference(self):
        """Return the circumference of the circle."""
        return 2 * math.pi * self.radius


class Triangle(Shape):
    def __init__(self, A, B, C):
        self.A = A
        self.B = B
        self.C = C

    def __str__(self):
        return f'Triangle({self.A}, {self.B}, {self.C})'

    def circumference(self):
        return (self.A.distance(self.B) + self.B.distance(self.C) +
                self.C.distance(self.A))

    def area(self):
        """
        Return the area of a triangle.

        The area is defined as area = base * height / 2. Alternatively,
        a * b * sin(gamma) / 2 can be used.
        See http://en.wikipedia.org/wiki/Triangle#Using_trigonometry and
        http://en.wikipedia.org/wiki/Angle#Dot_product_and_generalisation.
        """
        a = self.C.distance(self.B)
        b = self.C.distance(self.A)
        Vector = collections.namedtuple('Vector', ['x', 'y'])
        CB = Vector(self.B.x - self.C.x, self.B.y - self.C.y)
        CA = Vector(self.A.x - self.C.x, self.A.y - self.C.y)
        CA_dot_CB = CA.x * CB.x + CA.y * CB.y
        gamma = math.acos(CA_dot_CB / (a * b))
        return a * b * math.sin(gamma) / 2


if __name__ == '__main__':
    a_triangle = Triangle(Point(0, 0), Point(4, 0), Point(0, 3))
    a_circle = Circle(Point(25, 15), 7)
    shapes = [a_triangle, a_circle]
    for shape in shapes:
        print(shape)
        print(f'area: {shape.area()}, circumference: {shape.circumference()}')
