#!/usr/bin/env python3
"""
Implementation of a Circle class offering area and circumference methods.
"""
import math


class Circle:
    def __init__(self, x, y, radius):
        self.x = x
        self.y = y
        self.radius = radius

    def __str__(self):
        return (f'Circle(x={self.x}, y={self.y}, radius={self.radius})')

    def area(self):
        """Return the area of the circle."""
        return math.pi * self.radius * self.radius

    def circumference(self):
        """Return the circumference of the circle."""
        return 2 * math.pi * self.radius


if __name__ == '__main__':
    a_circle = Circle(25, 15, 7)
    print(a_circle)
    print(f'area: {a_circle.area():.2f}, '
          f'circumference: {a_circle.circumference():.2f}')
