# after refacturing
def add_to(n):
    """
    Return the sum of all values from 1 to n.
    """
    return n * (n + 1) // 2
