#!/usr/bin/env python3

name = 'Pat'
points = 3
print(f'{name} received {points} points.')
value = 3.1473
s = f'The formatted value is {value:.2f}.'
print(s)
