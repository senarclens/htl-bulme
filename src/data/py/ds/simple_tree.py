class TreeNode(object):
    """
    Simple implementation of a binary tree.
    """
    def __init__(self, data, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

# populating the tree
root = TreeNode(10)
root.left = TreeNode(5)
root.right = TreeNode(30)
root.left.left = TreeNode(2)
root.left.right = TreeNode(8)
root.right.left = TreeNode(21)

def print_tree(tree):
    """
    Print the elements of a tree in no specific order.
    """
    if tree:
        print(tree.data)
        print_tree(tree.left)
        print_tree(tree.right)
print_tree(root)
