class ListNode(object):
    """
    Simple implementation of a singly linked list node.
    """
    def __init__(self, data, succ=None):
        self.data = data
        self.succ = succ

# populating the list
head = ListNode(4)
tail = head
for i in range(3, 0, -1):
    tail.succ = ListNode(i)
    tail = tail.succ

# using the list (eg. print its elements)
list_iterator = head
while list_iterator:
    print(list_iterator.data)
    list_iterator = list_iterator.succ
