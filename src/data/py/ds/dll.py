class ListNode(object):
    """
    Simple implementation of a doubly linked list node.
    """
    def __init__(self, data, pred=None, succ=None):
        """
        `succ`: the current node's successor
        `pred`: the current node's predecessor
        """
        self.data = data
        self.pred = pred
        self.succ = succ

# populating the list
head = ListNode(10)
tail = head
for i in range(9, 0, -1):
    tail.succ = ListNode(i, tail)
    tail = tail.succ

# iterate over the list
list_iterator = head
while list_iterator:
    print(list_iterator.data)
    list_iterator = list_iterator.succ

# iterate from the back
list_iterator = tail
while list_iterator:
    print(list_iterator.data)
    list_iterator = list_iterator.pred
