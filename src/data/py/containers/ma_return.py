#!/usr/bin/env python3

def read_result(result):
    name, score = result.split(", ")
    return name, int(score)  # tuple is created "on the fly"

# ...

result = "Joe, 117"
name, score = read_result(result)  # tuple is unpacked into separate variables
