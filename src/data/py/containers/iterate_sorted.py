#!/usr/bin/env python3

d = {"telephone": "+43(0)316/380-1234",
     "email": "contact@uni-graz.at",
     "office": "Resowi, E3"}
for key in sorted(d.keys()):
    print(key, "--", d[key])
