s1 = {1, 4, 5, 3, 1, 3, 5, 4, 3, 1, 3, 4}
s2 = {7, 8, 3, 9, 2, 6}
intersection = s1 & s2  # s1.intersection(s2)
union = s1 | s2  # s1.union(s2)
difference = s1 - s2  # s1.difference(s2)

