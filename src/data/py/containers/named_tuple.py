#!/usr/bin/env python3

from collections import namedtuple
Point = namedtuple('Point', ['x', 'y'])  # define new named subclass of tuple
p = Point(11, y=22)  # instantiate with positional args or keywords
print(p[0] + p[1])  # indexable like a regular tuple
print(p.x + p.y)  # fields also accessible by name
