#!/usr/bin/env python3

import numpy as np
print(f'{np.zeros([3,4])=}')
a1 = np.ones([4,3])
print(f'{np.ones([4,3])=}')
print(f'{3 * np.ones([4,3])=}')
a = np.arange(15).reshape(3, 5)
m = np.matrix(np.arange(6).reshape(2, 3))
print(f'matrix {m=}')
print(f'array {a=}')
print(f'{m * a=}')

print(f'transposed:\n{m.T=}')
print(f'inverse:\n{m.I=}')
