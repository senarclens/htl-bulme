#!/usr/bin/env python3

d = {"telephone": "+43(0)316/380-1234",
     "email": "contact@uni-graz.at",
     "office": "Resowi, E3"}
for key in d:  # same as `for key in d.keys()`
    print(key, "--", d[key])
for value in d.values():
    print(value)
for key, value in d.items():
    print(key, "--", value)
