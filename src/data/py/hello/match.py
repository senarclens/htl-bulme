#!/usr/bin/env python3

LANG = 'en'
# ...
match LANG:
    case 'en':
        output = 'Good Morning!'
    case 'de':
        output = 'Guten Morgen!'
    case _:
        output = 'Bonjour!'
print(output)
