#!/usr/bin/env python3

SECRET = 4  # guaranteed random - chosen via rolling a die

while True:
    text = input("Guess an integer: ")
    number = int(text)

    if number == SECRET:
        print("you win")
        break
    elif number < SECRET:
        print("you guessed too low")
    elif number > SECRET:
        print("you guessed too high")
