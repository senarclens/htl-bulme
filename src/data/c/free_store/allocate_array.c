#include <stdio.h>
#include <stdlib.h>
int main(void) {
  const size_t size = 12;  // any value determined at runtime
  int* array = (int*) malloc(size * sizeof(int));
  if (array == NULL) { exit(EXIT_FAILURE); }
  for (size_t i = 0; i < size; ++i) {
    array[i] = i * i;  // initialize the array ...
  }
  for (size_t i = 0; i < 3; ++i) {  // use the array ...
    printf("array[%lu]: %d\n", i, array[i]);
  }
  free(array);  // otherwise memory will grow when the program keeps running
  return 0;
}
