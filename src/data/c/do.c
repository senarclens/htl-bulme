#include <stdio.h>

int main() {
  int condition = 0;
  do {
    printf("Do this at least once.\n");
  } while (condition);
  printf("FIRE!\n");
  return 0;
}
