#include <stdio.h>
#include <stdlib.h>

typedef struct intarray IntArray;
struct intarray {
  size_t reserved;  // available storage
  size_t elements;  // current number of elements
  int* array;

  void (*push_back)(IntArray* self, int elem);
  long int (*sum)(const IntArray* self);
  void (*print)(const IntArray* self);
  IntArray* (*destruct)(IntArray* self);
};

IntArray* ia_destruct(IntArray* self) {
  free(self->array);
  self->array = (int*) NULL;
  free(self);
  return (IntArray*) NULL;
}

void ia_push_back(IntArray* self, int elem) {
  if (self->elements == self->reserved) {
    self->reserved *= 2;
    self->array = realloc(self->array, self->reserved * sizeof(int));
  }
  self->array[self->elements] = elem;
  self->elements++;
}

long int ia_sum(const IntArray* self) {
  long int sum = 0;
  for (size_t i = 0; i < self->elements; ++i) {
    sum += self->array[i];
  }
  return sum;
}

void ia_print(const IntArray* self) {
  if (!self->elements) {
    printf("[]\n");
  } else {
    printf("[");
    for (size_t i = 0; i < self->elements - 1; ++i) {
      printf("%d,", self->array[i]);
    }
    printf("%d]\n", self->array[self->elements - 1]);
  }
}

IntArray* ia_construct() {
  IntArray* self = (IntArray*) malloc(sizeof(IntArray));
  self->reserved = 100;
  self->elements = 0;
  self->array = (int*) malloc(self->reserved * sizeof(int));
  self->print = ia_print;
  self->sum = ia_sum;
  self->push_back = ia_push_back;
  self->destruct = ia_destruct;
  return self;
}

int main(void) {
  IntArray* ia = ia_construct();
  ia->push_back(ia, 10);
  ia->push_back(ia, 3);
  ia->push_back(ia, 7);
  ia->push_back(ia, 12);
  ia->push_back(ia, 8);
  ia->print(ia);
  printf("The sum of all elements is %ld.\n", ia->sum(ia));
  ia = ia->destruct(ia);
  return 0;
}
