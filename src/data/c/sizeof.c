#include <stdio.h>
#include <stdint.h>

int main(void) {
  char c = 97;  // 97 == 'a'
  printf("char\t: \%zu Byte\n" , sizeof(char));
  printf("uint32_t: \%zu Bytes\n", sizeof(uint32_t));
  printf("int32_t\t: \%zu Bytes\n", sizeof(int32_t));
  printf("uint64_t: \%zu Bytes\n", sizeof(uint64_t));
  printf("int64_t\t: \%zu Bytes\n", sizeof(int64_t));

  // do the same for short, int, unsigned int, long int,
  // float and double
  // compile the program and try to understand the ouput

  printf("short\t: \%zu Bytes\n", sizeof(short));
  printf("int\t: \%zu Bytes\n", sizeof(int));
  printf("unsigned: \%zu Bytes\n", sizeof(unsigned int));
  printf("long\t: \%zu Bytes\n", sizeof(long int));
  printf("float\t: \%zu Bytes\n", sizeof(float));
  printf("double\t: \%zu Bytes\n", sizeof(double));

  printf("66\t: \%zu Bytes\n", sizeof(66));
  printf("\"Hello\"\t: \%zu Bytes\n", sizeof("Hallo"));
  printf("%c\t: \%zu Bytes\n", c, sizeof(c));
  printf("343434\t: \%zu Bytes\n", sizeof(343434));
  return 0;
}
