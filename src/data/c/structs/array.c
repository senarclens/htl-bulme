#include <stdio.h>
typedef struct {
  int id;
  char name[15];
  unsigned int grades[8];
} Student;

Student read_student(int id) {
  // get data from eg. a database
  Student s = { id, "Pat Kaling", { 1, 1, 2, 1, 3, 2, 1, 1 } };  // from a database
  return s;
}
int main() {
  Student s = read_student(123);
  printf("%s has a %u in English.", s.name, s.grades[3]);
  return 0;
}
