#include <stdio.h>

typedef struct {
  float x;
  float y;
} Point;

int main() {
  Point p1 = { .x=1.5 , .y=3.0 };
  Point p2 = p1;  // copy assignment
  p2.x = -5;  // does not affect `p1`
  return 0;
}
