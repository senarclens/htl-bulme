cmake_minimum_required(VERSION 3.18)
project(array_tool VERSION 1.0)
set(C_SRCS  # all c source files
  array.c
  main.c
  ui.c
)
set(CMAKE_C_STANDARD 11)
add_executable(${PROJECT_NAME} ${C_SRCS})
