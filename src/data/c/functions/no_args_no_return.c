#include <stdio.h>

void print_usage() {  // `void` means that nothing is returend
  puts("This function prints two lines of");
  puts("static text.");
}

int main() {
  puts("Before calling our function.");
  print_usage();  // Call a function without arguments
  puts("After calling our function.");
  return 0;
}
