#include <stdio.h>

void print_grade(char name[], int i) {  // function with two parameters
  printf("%s's grade is %d.\n", name, i);
}

int main() {
  puts("Before first function call.");
  print_grade("Banana Joe", 5);
  puts("Before second function call.");
  print_grade("Peppa Pig", 1);
  puts("After function calls.");
  return 0;
}
