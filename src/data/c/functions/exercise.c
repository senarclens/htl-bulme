#include <stdio.h>

double min(double first, double second) {
  return first < second ? first : second;
}
double max(double first, double second) {
  return first > second ? first : second;
}
int main() {
  printf("min(-3,  2) -> %lf\n", min(-3, 2));
  printf("min( 4, -5) -> %lf\n", min(4, -5));
  printf("min( 4,  5) -> %lf\n", min(4, 5));
  printf("max(-3,  2) -> %lf\n", max(-3, 2));
  printf("max( 4, -5) -> %lf\n", max(4, -5));
  printf("max( 4,  5) -> %lf\n", max(4, 5));
  return 0;
}
