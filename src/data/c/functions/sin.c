#include <math.h>
#include <stdio.h>

int main() {
  double result = sin(3.14159265359);
  printf("The sin of 3.14159265359 is %lf.\n", result);
  return 0;
}
