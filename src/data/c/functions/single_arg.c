#include <stdio.h>

void print_grade(int i) {  // `i` is a parameter to `print_grade`
  printf("The student's grade is %d.\n", i);
}

int main() {
  puts("Before first function call.");
  print_grade(5);  // `print_grade` gets called with the argument `5`
  puts("Before second function call.");
  print_grade(1);  // `print_grade` gets called with the argument `1`
  puts("After function calls.");
  return 0;
}
