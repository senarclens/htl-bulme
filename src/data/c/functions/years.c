#include <stdio.h>
int calculate_years(double investment, double goal, double interest_rate) {
  int years = 0;
  while (investment < goal) {
    years++;
    investment = investment * (1 + interest_rate);
  }
  return years;
}

int main() {
  printf("To turn 200 into 300 euros at 4%% interest, wait %d years.\n",
    calculate_years(200, 300, 0.04));
  printf("To turn 500 into 700 euros at 8%% interest, wait %d years.\n",
    calculate_years(500, 700, 0.08));
  return 0;
}
