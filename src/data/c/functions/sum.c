#include <stdio.h>

int sum(int first, int second) {
  int result = first + second;
  return result;
}

int main() {
  int total = sum(1, 2);
  printf("The calculated total is %d.\n", total);
  int price = sum(2, 3);
  printf("The calculated price is %d.\n", price);
  return 0;
}
