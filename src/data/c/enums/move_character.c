#include <stdio.h>

enum Orientation {
  NORTH,
  EAST,
  SOUTH,
  WEST,
};


int main() {
  enum Orientation heading = EAST;  // user controls movement of character
  switch (heading) {
  case NORTH:
    puts("Hero moves north...");  // in lieu of proper action
    break;
  case EAST:
    puts("Hero moves east...");
    break;
  case SOUTH:
    puts("Hero moves south...");
    break;
  case WEST:
    puts("Hero moves west...");
    break;
  }
  return 0;
}
