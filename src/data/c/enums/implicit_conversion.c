#include <stdio.h>

int main() {
  enum Color { RED, GREEN, BLUE };
  enum Color my_color = 1;  // Valid in C!  my_color is GREEN
  int color_int = my_color;  // Implicit conversion
  my_color = color_int + 10;  // Also valid for compiler! 💀
  printf("my_color: %d\n", my_color);  // definitely no color 💩
  return 0;
}
