#include <stdint.h>
#include <criterion/criterion.h>
#include <criterion/new/assert.h>
uint64_t add_to(uint32_t n);  // usually in header file

// `cr_expect` fails the test but continues the remaining assertions
// `cr_assert` leads to a failing test and terminates immediately
Test(sum_to, fast_test) {
  cr_expect(eq(u64, add_to(0), 0), "Sum of 0 is 0.");
  cr_expect(eq(u64, add_to(1), 1), "Sum of 1 is 1.");
  cr_expect(eq(u64, add_to(2), 3), "Sum of 1..2 is 3.");
  cr_expect(eq(u64, add_to(25), 325), "Sum of 1..25 is 325.");
  cr_expect(eq(u64, add_to(26), 351), "Sum of 1..26 is 351.");
  cr_expect(eq(u64, add_to(500000000), 125000000250000000),
    "This function must work for large unsigned integers.");
}

Test(sum_to, slow) {
  cr_expect(eq(u64, add_to(UINT32_MAX - 1), 9223372030412324865),
    "The function must work for `UINT32_MAX - 1`. Hint: double trouble ;)");
  cr_expect(eq(u64, add_to(UINT32_MAX - 2), 9223372026117357571),
    "add_to(UINT32_MAX - 2). Hint: floating-point arithmetic.");
}

// does not terminate for the original code due to an overflow bug of the
// loop counter (the loop would terminate only at UINT_MAX + 1)
Test(sum_to, edge_test) {
  cr_assert(eq(u64, add_to(UINT32_MAX), 9223372034707292160),
    "The function must work for the largest unsigned integer.");
}
