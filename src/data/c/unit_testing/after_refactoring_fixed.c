#include <stdint.h>
// Return the sum of all values from 1 to n.
uint64_t add_to(uint32_t n) {
  // avoid doubles as they cannot represent numbers close to UINT_MAX correctly
  if (n % 2) return ((n + 1ull) / 2) * n;
  return (n / 2 * (n + 1ull));
}
