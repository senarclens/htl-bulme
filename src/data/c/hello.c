#include <stdio.h>

/* Hello World program written in C. */
int main(void) {
  printf("Hello, World!\n");  // output some text
  return 0;  // 0 means success
}
