#include <stdio.h>
int main() {
  int num;
  FILE* f = fopen("data.txt","r");
  if(f == NULL) {
    fprintf(stderr, "Error!");
    return 1;
  }
  char line[100];  // a fixed size buffer for reading
  fgets(line, sizeof(line), f);
  sscanf(line, "%d\n", &num);
  printf("Value in the file times two: %d\n", num * 2);
  fgets(line, sizeof(line), f);
  puts(line);
  fclose(f);
  return 0;
}
