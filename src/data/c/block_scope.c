#include <stdio.h>

int main() {
  {
    int number = 3;
  }
  // printf("%d", number);  // error: number not declared in this scope
  return 0;
}
