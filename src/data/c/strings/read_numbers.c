#include <stdio.h>

int main() {
  char line[10];  // max. 9 characters + \0
  int first = 0; int second = 0;
  printf("enter two integers separated by a space (0 <= number <= 9999): ");
  fgets(line, sizeof(line), stdin);
  printf("You entered the text `%s`.\n", line);
  sscanf(line, "%d %d", &first, &second);
  printf("The numbers you entered are `%d` and `%d`.\n", first, second);
}
