#include <stdio.h>
#include <string.h>

int main() {
  char name[] = "Pat";
  char destination[10];  // programmer is responsible for destination's size
  strcpy(destination, name);
  printf("Destination is `%s`\n", destination);
  return 0;
}
