#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
  for (int i = 1; i < argc; ++i) {
    if (strcmp("-h", argv[i]) == 0 ||
        strcmp("--help", argv[i]) == 0) {
      puts("Usage: ls [OPTION]... ");
      puts("-h, --help display this help and exit");
      exit(0);
    }
  }
  puts("To solve the world's problems, use the correct arguments");
  return 0;
}
