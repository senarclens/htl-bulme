#include <stdint.h>
#include <stdio.h>

// Return the sum of all values from 1 to n.
uint64_t add_to_on(uint32_t n) {
  uint64_t sum = 0;
  // watch out: `i` has to be greater than UINT32_MAX if n is UINT32_MAX
  for (uint64_t i = 1; i <=n; ++i) {  //
    sum += i;
  }
  return sum;
}

uint64_t add_to_o1(uint32_t n) {
  // doubles cannot represent numbers close to UINT64_MAX correctly
  // division by `2.0` leads to wrong result if n is close to UINT32_MAX
  if (n % 2) return ((n + 1ull) / 2) * n;
  return (n / 2 * (n + 1ull));
}

void print_usage(char* command) {
  printf("Usage: %s {1|n}\n", command);
}

int main(int argc, char** argv) {
  uint32_t n = UINT32_MAX;
  if (argc != 2) { print_usage(argv[0]); return 1; }
  switch (*argv[1]) {
  case '1':
    printf("result: %ld\n", add_to_o1(n)); break;
  case 'n':
    printf("result: %ld\n", add_to_on(n)); break;
  default:
    print_usage(argv[0]); return 1;
  }
  return 0;
}
