#include <stdio.h>

int main(void) {
  int guess = 3;  // entered by the user
  if (guess == 4) {  // guaranteed random guess, used a die
    printf("You won!\n");
  } else {
    printf("Better luck next time, loser!\n");
  }
  return 0;
}
