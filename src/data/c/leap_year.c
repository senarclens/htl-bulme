#include <stdbool.h>
#include <stdio.h>

/*

  Test Cases

  input | result
  2000  | true
  2021  | false
     4  | true
   100  | false
  1996  | true
  1900  | false
  -100  | false
   -15  | false
  -800  | true
   -48  | true
*/

bool is_leap_year(int year) {
}

void test_is_leap_year() {
  int leap_years[]={4, 2000, 2020, 1996, -800, -48};
  int no_leap_years[]={123, 1900, 100, -100, -15, 2021};

  for(int x =0; x <6; x++){
    if (!is_leap_year(leap_years[x]))
      {printf("input: %d; FAIL\n" ,leap_years[x]);}
  }
  for(int x =0; x <6; x++){
    if (is_leap_year(no_leap_years[x]))
      {printf("input: %d; FAIL\n" ,no_leap_years[x]);}
  }
}

int main() {
  test_is_leap_year();
  return 0;
}
